// https://eslint.org/docs/user-guide/configuring

module.exports = {
	root: true,
	parserOptions: {
		parser: 'babel-eslint'
	},
	env: {
		browser: true,
	},
	"globals": {
		"$": true,
		"jQuery": true,
	},
	extends: [
		// https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
		// consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
		'plugin:vue/essential',
		// https://github.com/standard/standard/blob/master/docs/RULES-en.md
		'standard'
	],
	// required to lint *.vue files
	plugins: [
		'vue'
	],
	// add your custom rules here
	rules: {
		// allow async-await
		'generator-star-spacing': 'off',
		// allow debugger during development
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		"indent": ["error", "tab"],
		"quotes": 0,
		"no-else-return": 0,
		"guard-for-in": 0,
		"global-require": 0,
		"semi": ["error", "always", {"omitLastInOneLineBlock": true}],
		"comma-dangle": 0,
		"space-before-function-paren": 0,
		"padded-blocks": 0,
		"no-new": 0,
		"vue/no-use-v-if-with-v-for": "off",
		"import/no-webpack-loader-syntax": "off",
		"one-var": 0,
		"no-tabs": 0,
		"keyword-spacing": 0,
		"vue/no-parsing-error": [2, {"x-invalid-end-tag": false}]
	},

}


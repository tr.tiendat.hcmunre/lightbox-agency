const merge = require('webpack-merge');
const fs = require('fs');
const dotenv = require('dotenv');
// const envConfig = dotenv.config().parsed
const envConfig = dotenv.parse(fs.readFileSync(`.env.${process.env.NODE_ENV || 'development'}`));
const config = {};

for (let k in envConfig) {
	config[k] = `"${envConfig[k]}"`;
}

module.exports = merge(config, {
	NODE_ENV: 'production',
});

module.exports = {
	apps: [
		{
			name: "lightbox-agency",
			script: "./server",
			watch: true,
			env: {
				// "PORT": 8043,
				"NODE_ENV": "development"
			},
			env_staging: {
				// "PORT": 8043,
				"NODE_ENV": "staging"
			},
			env_production: {
				// "PORT": 8043,
				"NODE_ENV": "production",
			}
		}
	]
};

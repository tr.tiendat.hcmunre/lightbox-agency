import { name } from '../package.json';
export { name };

// Export environment
export const NODE_ENV_DEVELOPMENT = 'development';
export const NODE_ENV_STAGING = 'staging';
export const NODE_ENV_PRODUCTION = 'production';
export const env = process.env.NODE_ENV || NODE_ENV_DEVELOPMENT;

// Export server configurations
export const ip = process.env.IP || (env === NODE_ENV_DEVELOPMENT ? 'localhost' : undefined);
export const port = process.env.PORT || (env === NODE_ENV_PRODUCTION ? undefined : 8043);

// define server domains
const domains = {
    [NODE_ENV_DEVELOPMENT]: process.env.DOMAIN || 'localhost',
    [NODE_ENV_STAGING]: process.env.DOMAIN || 'agency.lightbox.success',
    [NODE_ENV_PRODUCTION]: process.env.DOMAIN || 'agency.lightbox.com',
};

// Export domain for environment
export const domain = domains[env];

// Export protocol
const enableHttps = false;
export const protocol = enableHttps ? 'https://' : 'http://';

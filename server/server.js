import path from 'path';
import express from 'express';
import favicon from 'serve-favicon';
import helmet from 'helmet';
import compression from 'compression';
// import fs from 'fs';

import { domain, port } from './config'; // env,

// create express app
const app = express();

// Use Helmet - security package
app.use(helmet());

// Use gzip compression
app.use(compression());

// serve built app and favicon
const publicPath = path.resolve(__dirname, '../dist');
app.use(express.static(publicPath));
app.use(favicon(publicPath + '/static/favicon.ico'));

// Router
app.get('*', function(req, res) {
    res.sendFile(publicPath + '/index.html');
});

// Start app
app.listen(port, function() {
    console.log('Server listening at: ' + domain + ':' + port);
});

export default app;

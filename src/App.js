"use strict";

import {mapActions, mapGetters} from 'vuex';
import {GETTERS, MUTATES, ACTIONS} from './store';
import {IS_IGNORE_COMMAND} from "./common/const/development.debug";

export default {
	// constructor() {
	// 	if (!this.isAuthenticated) {
	// 		this.loadMe();
	// 	}
	// },

	name: "App",
	data() {
		return {};
	},
	computed: {
		...mapGetters([GETTERS.LOADING, GETTERS.IS_AUTHENTICATED, GETTERS.CURRENT_USER])
	},
	async created() {
		if (!IS_IGNORE_COMMAND && !this.isAuthenticated) {
			this.$store.commit(MUTATES.LOADING);
			await this.loadMe().then(response => {
			});
			this.$store.commit(MUTATES.LOADED);
		} else {
		}
	},
	methods: {
		...mapActions([ACTIONS.LOAD_ME])
	}
};

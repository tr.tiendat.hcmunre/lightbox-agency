// css files

// import 'va/lib/css';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'datatables.net-bs/css/dataTables.bootstrap.css';
import 'select2/dist/css/select2.min.css';
import 'inputmask/css/inputmask.css';
import 'admin-lte/dist/css/AdminLTE.min.css';
import 'admin-lte/dist/css/skins/_all-skins.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'assets/scss/style.scss';
import 'admin-lte/plugins/iCheck/square/blue.css';
import 'admin-lte/plugins/timepicker/bootstrap-timepicker.min.css';
import 'admin-lte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css';

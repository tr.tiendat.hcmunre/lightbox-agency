// js files

// import 'va/lib/script';

import 'expose-loader?$!expose-loader?jQuery!jquery';
// import 'bootstrap';
import 'datatables.net-bs/js/dataTables.bootstrap';

import 'jquery-slimscroll';
import 'jquery-ui/ui/widgets/sortable.js';
import 'jquery-ui/ui/widgets/datepicker.js';
import 'jquery-ui/themes/base/datepicker.css';

import 'select2';
import 'admin-lte/plugins/input-mask/jquery.inputmask';
import 'admin-lte';
import 'admin-lte/plugins/iCheck/icheck';
import 'admin-lte/plugins/timepicker/bootstrap-timepicker.min';
import 'admin-lte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min';
import 'chart.js';

// import 'bootstrap3-wysihtml5-bower/dist/amd/bootstrap3-wysihtml5.all.js'
import 'assets/libraries/jquery/jquery.alterclass.js';
import 'assets/libraries/jquery/jquery.datatable.js';

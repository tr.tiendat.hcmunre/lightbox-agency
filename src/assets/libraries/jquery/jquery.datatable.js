//
// Pipelining function for DataTables. To be used to the `ajax` option of DataTables
//
$.fn.dataTable.pipeline = function (opts) {
	// Configuration options
	var conf = $.extend({
		pages: 5, // number of pages to cache
		url: '', // script url
		data: null, // function or object with parameters to send to the server
		// matching how `ajax.data` works in DataTables
		method: 'GET', // Ajax HTTP method,
	}, opts);

	// Private variables for storing the cache
	// var cacheLower = -1;
	// var cacheUpper = null;
	// var cacheLastRequest = null;
	// var cacheLastJson = null;

	return function (request, drawCallback, settings) {
		var requestStart = request.start;
		var requestLength = request.length;
		var fields = '';
		var sort = request.order[0].column;
		var order = request.order[0].dir;
		var dir = order === 'asc' ? '+' : '-';
		request.columns.forEach(function(item) {
			if (item.data) {
				fields += `${item.data} `;
			}
		});

		request.fields = fields.trimEnd(',');
		request.page = request.draw;
		request.search = request.search.value;
		request.sort = `${dir}${request.columns[sort].data}`;

		// requestStart = requestStart - (requestLength * (conf.pages - 1));
		// if (requestStart < 0) {
		// 	requestStart = 0;
		// }

		// request.start = requestStart;
		request.per_page = requestLength;
		request.page = Math.round(requestStart / requestLength) + 1;

		request.order = undefined;
		request.columns = undefined;
		request.draw = undefined;
		request.start = undefined;

		// Provide the same `data` options as DataTables.
		if (typeof conf.data === 'function') {
			// As a function it is executed with the data object as an arg
			// for manipulation. If an object is returned, it is used as the
			// data object to submit
			var d = conf.data(request);
			if (d) {
				$.extend(request, d);
			}
		} else if ($.isPlainObject(conf.data)) {
			// As an object, the data given extends the default
			$.extend(request, conf.data);
		}

		settings.jqXHR = $.ajax({
			"type": conf.method,
			"url": conf.url,
			"data": request,
			"dataType": "json",
			"cache": false,
			headers: {
				'Authorization': JSON.parse(localStorage.getItem('token'))
			},
			"xhrFields": {
				withCredentials: true
			},
			"success": function (json) {
				json.recordsTotal = json.total;
				json.recordsFiltered = json.total;
				drawCallback(json);
			}
		});
		settings.oLanguage.oPaginate.sNext = '<i class="fa fa-forward"></i>';
		settings.oLanguage.oPaginate.sPrevious = '<i class="fa fa-backward"></i>';
		settings.oLanguage.oPaginate.sFirst = '<i class="fa fa-fast-forward"></i>';
		settings.oLanguage.oPaginate.sLast = '<i class="fa fa-fast-backward"></i>';
	};
};

// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register('clearPipeline()', function () {
	return this.iterator('table', function (settings) {
		settings.clearCache = true;
	});
});

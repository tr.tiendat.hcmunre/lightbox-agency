/**
 * Use the VueI18h plugins for translate languages
 * Document reference: https://kazupon.github.io/vue-i18n/
 */
import Vue from "vue";
import VueI18n from 'vue-i18n';
import en from './en';

Vue.use(VueI18n);

const defaultLocale = 'en';

/**
 * Ready translated locale messages
 * For example English:
 * @en: english locale from a json file
 */
const languages = {
	en,
};

const messages = Object.assign((languages));

/**
 * Create VueI18n instance with options
 * @locale: set default locale
 * @messages: set locale messages
 */
export const i18n = new VueI18n({
	locale: defaultLocale,
	messages,
});

import {i18n} from './index';

export const setLocale = {
	validationFields(...fields) {

		const validationCustomizeMessage = {};

		fields.map(field => {
			validationCustomizeMessage[field] = this[field];
		});
		return validationCustomizeMessage;
	},
	common: {
		notify: {
			success: (name, action) => i18n.t('common.notify.success', {name, action: action.charAt(0).toUpperCase() + action.slice(1)})
		},
		messages: {
			delete: name => i18n.t('common.messages.delete', {name}),
			cannot_delete: name => i18n.t('common.messages.cannot_delete', {name})
		}
	},
	LogIn: {
		validation: {
			password: {
				required: i18n.t('login.validation.password.required'),
				regex: i18n.t('login.validation.password.regex')
			},
			email: {
				required: i18n.t('login.validation.email.required'),
				email: i18n.t('login.validation.email.email')
			},
		},
		notify: {
			success: {
				emailVerify: email => i18n.t('login.notify.success.email_verify', {email})
			}
		}
	},
	Register: {
		validation: {
			first_name: {
				required: i18n.t('register.validation.first_name.required'),
			},
			last_name: {
				required: i18n.t('register.validation.last_name.required'),
			},
			agree_term: {
				truthy: i18n.t('register.validation.agree_term.truthy')
			},
			password: {
				required: i18n.t('register.validation.password.required'),
				regex: i18n.t('register.validation.password.regex')
			},
			confirmed_password: {
				required: i18n.t('register.validation.confirmed_password.required'),
				confirmed: i18n.t('register.validation.confirmed_password.confirmed')
			},
			email: {
				required: i18n.t('register.validation.email.required'),
				email: i18n.t('register.validation.email.email')
			},
			phone: {
				required: i18n.t('register.validation.phone.required'),
			},
			company_name: {
				required: i18n.t('register.validation.company_name.required'),
			},
			address: {
				required: i18n.t('register.validation.address.required'),
			},
			time_zone: {
				required: i18n.t('register.validation.time_zone.required'),
			},
			card_number: {
				required: i18n.t('register.validation.card_number.required'),
			},
			expired: {
				required: i18n.t('register.validation.expired.required'),
			},
			secret_code: {
				required: i18n.t('register.validation.secret_code.required'),
			},
			post_code: {
				required: i18n.t('register.validation.post_code.required'),
			},
			country: {
				required: i18n.t('register.validation.country.required'),
			}
		},
		notify: {
			success: {
				account: i18n.t('register.notify.success.account')
			}
		}
	},
	Profile: {
		notify: {
			success: {
				updated: i18n.t('profile.notify.success.updated')
			}
		}
	},
	Client: {
		validation: {
			name: {
				required: i18n.t('client.validation.name.required'),
				regex: i18n.t('client.validation.name.regex')
			}
		}
	},
	file: i18n.t('file'),
	location: i18n.t('location'),
	name: i18n.t('name'),
	client: i18n.t('client'),
	times: i18n.t('fromTime'),
	delete_all: i18n.t('delete_all'),
	load_data: i18n.t('load_data'),
	change_status: (status, subject) => i18n.t('change_status', { status, subject }),
	email_exists: i18n.t('email_exists'),
	leave: i18n.t('leave'),
	budget: i18n.t('budget'),
	fromDate: i18n.t('fromDate'),
	fromTime: i18n.t('fromTime')
};

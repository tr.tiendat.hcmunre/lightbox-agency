export const BASE_API_URL = process.env.VUE_APP_BASE_API;

export const API_URL = {
	// Guest constant
	LOGIN: 'signin',
	REGISTER: 'signup',
	SIGNOUT: 'signout',
	SIGNIN: 'signin',
	USER_EXISTS: 'user-exists',

	// clients constant
	CLIENT: 'clients',

	// ads constant
	AD: 'ads',
	LOCATION: 'ad-locations',
	TIME: 'ad-times',
	AGENCIES: 'agencies',

	// Current user constant
	PROFILE: 'me',
	AVATAR: 'avatar',

	// common constant
	DISTRICT: 'districts',
	TIME_ZONES: 'timezones',
	COUNTRY: 'countries',
	STATE: 'states',
	CITY: 'cities',
	LOCATION_CITY: 'location/cities',
	LOCATION_DISTRICT: 'location/districts',
	DEVICE: 'devices',
};

export const REQUEST_ACTION = {
	ADD: 'add',
	EDIT: 'edit',
	UPDATE: 'update',
	DELETE: 'delete',
	REMOVE: 'remove'
};

export const REQUEST_TYPE = {
	GET: 'GET',
	POST: 'POST',
	PUT: 'PUT',
	DEL: 'DELETE',
};

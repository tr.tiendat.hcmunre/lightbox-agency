export const DATEPICKER_FORMAT = process.env.DATEPICKER_FORMAT;
export const DATE_FORMAT = process.env.DATE_FORMAT;
export const DATETIME_FORMAT = process.env.DATETIME_FORMAT;
export const APP_TITLE = process.env.APP_TITLE;

export const AD_SETTING_MANAGEMENT = 'Ad Setting';
export const LOCATION_MANAGEMENT = 'Location Customization';
export const NEARBY_MANAGEMENT = 'Nearby Customization';
export const TIME_MANAGEMENT = 'Time Customization';
export const STATUS_MANAGEMENT = 'Status Customization';

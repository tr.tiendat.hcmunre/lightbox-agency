// App constants
export const APP_NAME = 'Lightbox';
export const SET_PAGE_TITLE = 'setPageTitle';
export const SET_BREADCRUMB = 'setBreadcrumb';
export const SET_TITLE = 'setTitle';

// Containers constant
export const DASHBOARD = 'Dashboard';
export const TERMS_CONDITIONS = 'Terms & Conditions';
export const PROFILE = 'My Profile';
export const CLIENTS_EDITION = 'Edit client';
export const CLIENTS_MANAGEMENT = 'Clients management';
export const CLIENTS_CREATION = 'Add client';
export const ADS_MANAGEMENT = 'Ads Management';
export const ADS_CREATION = 'Add Ad';
export const ADS_EDITION = 'Edit Ad';

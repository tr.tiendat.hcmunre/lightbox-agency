import * as time from './timeToString';

export default {
	install(Vue) {
		Vue.filter('timeToString', {
			read: time.time2String,
			write: time.stringToTime
		});
	}
};

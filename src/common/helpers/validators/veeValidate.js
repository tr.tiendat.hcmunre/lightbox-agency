import Vue from 'vue';
import { Validator, install as VeeValidate } from 'vee-validate';

Validator.extend('truthy', {
	getMessage: (field) => {
		return 'The ' + field + ' value is not truthy.';
	},
	validate: (value) => {
		return !!value;
	}
});

Validator.extend('less_than_eq', {
	getMessage: (field, args) => {
		return 'The ' + field + ' is must be less than or equal.';
	},
	validate: (value, {compareValue} = {}) => {
		let v1 = value,
			v2 = compareValue;

		if(typeof value === 'string') {
			v2 = parseInt(value);
		}
		if(typeof compareValue === 'string') {
			v2 = parseInt(compareValue);
		}
		return v1 <= (v2 || 0);
	}
}, {
	hasTarget: true,
	paramNames: ['compareValue']
});

Vue.use(VeeValidate, {
	events: ''
});

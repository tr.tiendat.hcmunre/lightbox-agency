export const commonMixin = {
	methods: {
		randomNumber(min, max) {
			min = Math.ceil(min);
			max = Math.floor(max);
			return Math.floor(Math.random() * (max - min + 1)) + min;
		},
		formatNumber(num, pattern = /\B(?=(\d{3})+(?!\d))/g) {
			const str = num.toString();
			return str.replace(pattern, ",");
		},
	}
};

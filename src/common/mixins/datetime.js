import moment from 'moment';

const pad = (num, size) => {
	return (1e15 + num + "").slice(-size);
};

export const dateTimeMixin = {
	methods: {
		dateFormat(value, format = 'DD/MM/YYYY') {
			return this.$options.filters.dateFormat(value, format);
		},
		stringToDate(value, format) {
			if (!value) return null;
			return +moment.utc(value, format);
		},
		timeFormat(value) {
			return this.$options.filters.timeFormat(value);
		},
		stringToTime(value) {
			if (!value) return null;
			const [hour, minute] = value.split(':');
			return parseInt(hour) * 60 + parseInt(minute);
		},
	},
	filters: {
		timeFormat(value) {
			if (!value) return '00:00';
			return`${parseInt(value / 60)}:${pad(value % 60, 2)}`;
		},
		dateFormat(value, format = 'DD/MM/YYYY') {
			if (!value) return moment.utc(value).format(format);
			return moment.utc(value).format(format);
		}
	}
};

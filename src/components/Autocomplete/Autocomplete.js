
export default {
	name: 'm-autocomplete',
	model: {
		prop: 'value',
		event: 'change'
	},
	props: {
		placeholder: {
			type: String,
		},
		disabled: {
			type: Boolean,
			default: false
		},
		outlined: {
			type: Boolean,
			default: false
		},
		name: {
			type: String,
			default: ''
		},
		width: {
			type: Number,
			default: 400
		},
		id: {
			type: String,
			default: `m-select-${Date.now()}`
		},
		ariaDescribedby: {
			type: String,
			default: ''
		},
		valid: {
			type: Boolean,
			default: true
		},
		required: {
			type: Boolean,
			default: false
		},
		options: {
			type: Array,
			default: () => {
				return [];
			},
			// required: true
		},
		mappings: {
			type: Object,
			default: () => ({})
		},
		url: String,
		value: null
	},
	data () {
		return {
			menuAnchor: null,
			menu: null,
			inputText: null,
			items: [],
			menuWidth: 400,
			isMenuOpen: false,
			menuCss: {
				'transform-origin': 'center top',
				'max-height': '422.5px'
			},
			searchText: '',
			// selectedText: '',
			selected: {},
			isOnSelected: false,
			isOnSearch: false
		};
	},
	computed: {
		classes () {
			return {
			};
		},
		ariaLabelledby () {
			let ret = this.id;
			if (this.$slots['label'] && this.$slots['label'].length === 1) {
				ret = ret + ' ' + this.$slots['label'][0].data.attrs.id;
			}
			return ret;
		},
		displayText() {},
		menuClasses() {
			return {
				'mdc-menu-surface--open': this.isMenuOpen
			};
		},
		filteredItems() {
			if(!this.searchText) {
				return this.items;
			}
			return this.items.filter(item => {
				return (item[this.mappings.text] || item.text || '').toLowerCase().includes(this.searchText.toLowerCase());
			});
		},
		selectedText: {
			get() {
				return this.isOnSearch ? this.searchText : this.getText(this.selected);
			},
			set(val) {
				if(this.isOnSelected) {
					this.isOnSelected = false;// reset selected
					return;
				}

				this.searchText = val;
				this.isOnSelected = false;// reset selected
				this.showMenu();
			}
		}

	},
	watch: {
		options() {
			this.items = this.options;
		},
		value() {
			this.setSelected();
		}
	},
	updated() {
		this.updateMenuUI();
	},
	mounted () {
		this.items = this.options;
		this.setSelected();
		this.menuAnchor = this.$refs.menuAnchor.$el;
		this.menu = this.$refs.mMenu.$el;
		this.inputText = this.$refs.mTextField.$el;
		this.$refs.mMenu.$el.style.width = this.$refs.mTextField.$el.clientWidth + 'px';
	},
	methods: {
		setSelected() {
			if(this.isOnSelected || this.isOnSearch) {
				return;
			}
			this.selected = this.items.find(item => this.getId(item) === this.value);
		},
		onChange (event) {
			this.$emit('change', event.detail);
			this.$emit('model', event.detail.value);
		},
		onSelect(event, item) {
			this.isOnSelected = true;
			this.selected = item;
			this.searchText = '';
			this.isOnSearch = false;
			this.isOnSelected = false;
			this.$emit('change', this.getId(item));
			this.$emit('model', this.getId(item));
		},
		onFocus(event, isSearch) {
			this.isOnSearch = isSearch;
			isSearch && (this.selectedText = '');
			this.showMenu(event.clientY);
			this.isMenuOpen = true;
		},
		onBlur(event) {
			let that = this;
			this.isOnSearch = false;
			setTimeout(() => {
				that.hideMenu();
			}, 200);
			event.preventDefault();

		},
		showMenu(positionY = 0) {
			this.menu.classList.add('mdc-menu-surface--open');
			this.menuCss.left = `${this.menuAnchor.offsetLeft}px`;
			positionY = positionY || this.menuAnchor.offsetTop - window.scrollY;

			let isTop = positionY + this.menu.clientHeight > window.innerHeight;

			if(isTop) {
				this.menuCss.top = `${this.inputText.offsetTop - this.menu.clientHeight}px`;
			}else {
				this.menuCss.top = `${this.menuAnchor.offsetTop}px`;
			}
		},
		hideMenu() {
			this.isMenuOpen = false;
			this.menu.classList.remove('mdc-menu-surface--open');
		},
		updateMenuUI() {
			let positionY = this.menuAnchor.offsetTop - window.scrollY;

			let isTop = positionY + this.menu.clientHeight > window.innerHeight;

			if(isTop) {
				this.menu.style.top = `${this.inputText.offsetTop - this.menu.clientHeight}px`;
			}else {
				this.menu.style.top = `${this.menuAnchor.offsetTop}px`;
			}
		},
		getId(item) {
			return item[this.mappings.id] || item.id || '';
		},
		getText(item) {
			return item[this.mappings.text] || item.text || '';
		},
		isSelected(item) {
			return this.getId(item) === this.getId(this.selected);
		}
	}
};

"use strict";
import { MDCCheckbox } from '@material/checkbox';


export default {
	name: 'm-checkbox',
	model: {
		prop: 'checked',
		event: 'change'
	},
	props: {
		id: '',
		value: {
			type: String
		},
		checked: {
			default: false
		},
		indeterminate: {
			type: Boolean,
			default: false
		},
		disabled: {
			default: false
		}
	},
	data: () => ({
		mdcCheckbox: undefined
	}),
	computed: {
		model: {
			get () {
				return this.checked;
			},
			set (state) {
				this.$emit('change', state);
			}
		},
		isChecked() {
			if (this.checked instanceof Array) {
				return this.checked.includes(this.value);
			} else {
				return this.checked;
			}
		}
	},
	watch: {
		// checked (value) {
		// 	if (value) {
		// 		this.mdcCheckbox.indeterminate = false;
		// 	}
		// },
		// indeterminate (value) {
		// 	this.mdcCheckbox.indeterminate = value
		// 	if (this.model && value) {
		// 		this.model = false;
		// 	}
		// },
		disabled (value) {
			this.mdcCheckbox.disabled = value;
		}
	},
	mounted() {
		this.mdcCheckbox = MDCCheckbox.attachTo(this.$el);
		// this.mdcCheckbox.checked = this.isChecked;
		this.mdcCheckbox.disabled = this.disabled;
		this.mdcCheckbox.indeterminate = this.indeterminate;
	},
	methods: {
	},
	beforeDestroy () {
		this.mdcCheckbox.destroy();
	}
};

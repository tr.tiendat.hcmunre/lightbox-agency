export default {
	name: 'Collapse',
	props: {
		name: String,
		description: String,
		showed: Boolean,
	},
	data: () => ({
		isShow: false,
	}),
	methods: {
		onToggleShow(evt) {
			this.isShow = !this.isShow;
		}
	}
};

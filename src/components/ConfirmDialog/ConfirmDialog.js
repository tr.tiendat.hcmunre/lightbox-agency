export default {
	model: {
		prop: 'open',
		event: 'change'
	},
	props: {
		open: {
			type: Boolean,
			default: false
		},
		message: {
			type: String,
			default: ''
		},
		buttonYesText: {
			type: String,
			default: ''
		},
		buttonNoText: {
			type: String,
			default: ''
		}
	},
	computed: {
		yesText() {
			return this.buttonYesText || 'Yes';
		},
		noText() {
			return this.buttonNoText || 'No';
		}
	},
	methods: {
		doAction(action){
			this.$emit('change', false);
			this.$emit(action);
		}
	}
};

export default {
	name: 'Input',
	model: {
		prop: 'value',
		event: 'model'
	},
	props: {
		placeholder: String,
		value: Number,
	},
	computed: {
		dataValue: {
			get() {
				return this.value;
			},
			set(val) {

				let regx = new RegExp(/^\d*\.?\d*$/);
				let regx1 = new RegExp(/^\d*\.$/);
				const newValue = parseFloat(val);
;
				if(regx.test(val) && val && !regx1.test(val)) {
					this.$emit('change', newValue);
					this.$emit('model', newValue);
				}else if(!regx.test(val) && val && isNaN(newValue)) {
					this.$el.querySelector('input').value = '';
				}else if(!regx.test(val)) {
					this.$el.querySelector('input').value = this.value;
				}
			}
		}
	},
	methods: {
		onIncrement() {
			this.dataValue = parseInt(this.dataValue) + 1;
		},
		onDecrement() {
			this.dataValue = parseInt(this.dataValue) - 1;
		},
	}
};

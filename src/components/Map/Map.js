import { LMap, LTileLayer, LMarker, LIcon } from "vue2-leaflet";
import { latLngBounds, latLng, icon } from "leaflet";
import 'leaflet/dist/leaflet.css';
import markerIcon from '@/assets/images/icons/location-icon.png';

export default {
	name: "m-map",
	props: {
		markers: {
			type: Array,
			default: () => ([])
		},
		center: {
			type: Array,
			default: () => ([])
		},
		bounds: [],
		height: {
			type: String,
			default: '400px'
		}
	},
	components: {
		LMap,
		LTileLayer,
		LMarker,
		LIcon
	},
	data() {
		return {
			zoom: 10,
			defaultCenter: latLng(43.7170226, -79.4197830350134),
			// defaultCenter: latLng(10.7758439, 106.7017555),
			defaultBounds: latLngBounds([[41.6765556, 83.3362128], [-141.00275, -52.3231981]]),
			url: "http://{s}.tile.osm.org/{z}/{x}/{y}.png",
			// url: "https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png",
			// url: "https://api.tiles.mapbox.com/v4/mapbox.light/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWJvdWNoYXVkIiwiYSI6ImNpdTA5bWw1azAyZDIyeXBqOWkxOGJ1dnkifQ.qha33VjEDTqcHQbibgHw3w",
			attribution:
				'&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
			icon: icon({
				iconUrl: markerIcon,
				iconSize: [22, 37],
				iconAnchor: [16, 37]
			}),
			staticAnchor: [32, 37],
			iconSize: 64,
			markerIcon
		};
	},
	computed: {
		dynamicAnchor() {
			return [this.iconSize / 2, this.iconSize * 1.15];
		},
		centerMap() {
			return this.center && this.center.length ? latLng(this.center[0], this.center[1]) : this.defaultCenter;
		},
		boundsMap() {
			return this.center ? latLngBounds([this.bounds[0], this.bounds[1]]) : [];
		}
	},
	methods: {}
};

import _ from 'lodash';

export default {
	name: 'menu-item',
	data: () => ({
		// isOpen: false
	}),
	props: {
		icon: {
			type: String,
			default: ''
		},
		name: {
			type: String
		},
		items: {
			type: Array,
			default () {
				return [];
			}
		},
		router: {
			type: Object,
			default () {
				return {
					name: ''
				};
			}
		},
		link: {
			type: String,
			default: ''
		}
	},
	created () {

	},
	computed: {
		isActive() {
			// let route = _.findLast(this.$router.options.routes, ['name', 'Home']);
			return this.router.name === this.$route.name || this.items.some(item => item.router.name === this.$route.name);
		}
	}
};

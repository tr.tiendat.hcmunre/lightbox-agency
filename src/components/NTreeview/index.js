import './styles.scss';
import Collapse from '@/components/Collapse/index.vue';

export default {
	name: 'NTreeview',
	model: {
		prop: 'selectedItems',
		event: 'model'
	},
	props: {
		items: Array,
		selectedItems: Array,
	},
	data: () => ({
		nodes: [],
		isLoading: false,
		tree: [],
		types: []
	}),
	created() {
		this.nodes = this.$props.items;
	},
	components: {
		Collapse
	},
	computed: {
		dataItems() {
			const children = this.types.map(type => ({
				id: type,
				name: this.getName(type),
				children: this.getChildren(type)
			}));

			return [{
				id: 1,
				name: 'All',
				children
			}];
		},
		selections() {
			const selections = [];

			for (const leaf of this.tree) {
				const node = this.nodes.find(node => node.id === leaf);

				if (!node) continue;

				selections.push(node);
			}

			return selections;
		},
		shouldShowTree() {
			return this.nodes.length > 0 && !this.isLoading;
		}
	},

	watch: {
		items() {
			this.nodes = this.items;
		},
		nodes(val) {
			this.types = val.reduce((acc, cur) => {
				const type = cur.type;

				if (!acc.includes(type)) acc.push(type);

				return acc;
			}, []).sort();
		},
		selections (val) {
			this.$emit('model', val);
		}
	},

	methods: {
		getChildren(type) {
			const nodes = [];

			for (const node of this.nodes) {
				if (node.type !== type) continue;

				nodes.push({
					...node,
					name: this.getName(node.name)
				});
			}

			return nodes.sort((a, b) => {
				return a.name > b.name ? 1 : -1;
			});
		},
		getName(name) {
			return name;
		}
	}
};

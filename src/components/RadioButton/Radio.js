"use strict";

export default {
	name: 'm-radio',
	model: {
		prop: 'modelValue',
		event: 'change'
	},
	props: {
		id: '',
		value: {
			type: String
		},
		modelValue: {
			default: false
		},
		checked: {
			default: false
		}
	},
	data: () => ({
	}),
	computed: {
		shouldBeChecked() {
			return this.modelValue === this.value;
		},
	},
	mounted() {
	},
	methods: {
		updateInput(event) {
			let isChecked = event.target.checked;

			this.$emit('change', this.value);

			this.$emit('changed', {
				checked: isChecked,
				value: this.value
			});
		}
	}
};

import { MDCSelect } from '@material/select';

export default {
	model: {
		prop: 'value',
		event: 'input'
	},
	props: {
		disabled: {
			type: Boolean,
			default: false
		},
		outlined: {
			type: Boolean,
			default: true
		},
		enhanced: {
			type: Boolean,
			default: false
		},
		name: {
			type: String,
			default: ''
		},
		width: {
			type: Number,
			default: 400
		},
		id: {
			type: String,
			default: `m-select-${Date.now() + Math.random()}`
		},
		ariaDescribedby: {
			type: String,
			default: ''
		},
		valid: {
			type: Boolean,
			default: true
		},
		required: {
			type: Boolean,
			default: false
		},
		placeholder: {
			type: String,
			default: ''
		},
		float: {
			type: Boolean,
			default: true
		},
		options: {
			type: Array,
			default: () => ([])
		},
		mapping: {
			type: Object,
			default: () =>({})
		},
		value: {
			default: ''
		}
	},
	data () {
		return {
			mdcSelect: undefined,
			slotObserver: undefined,
			menuWidth: 400,
			mdcMenu: undefined,
			items: []
		};
	},
	computed: {
		classes () {
			return {
				'mdc-select--disabled': this.disabled,
				'mdc-select--outlined': this.outlined,
				'mdc-select--with-leading-icon': this.$slots.leadingIcon,
				'mdc-select--required': this.required,
				'mdc-notched-placeholder': !this.float
			};
		},
		ariaLabelledby () {
			let ret = this.id
			if (this.$slots['label'] && this.$slots['label'].length === 1) {
				ret = ret + ' ' + this.$slots['label'][0].data.attrs.id;
			}
			return ret;
		},
		hasData() {
			return this.options.length;
		},
		selectedItem() {
			return this.getItem(this.value) || {};
		}
	},
	watch: {
		valid () {
			this.mdcSelect.valid = this.valid;
		},
		required () {
			this.mdcSelect.required = this.required;
		},
		options() {
			this.updateLabel();
		},
		value() {
			this.updateLabel();
		}
	},
	updated() {
		this.updateLabel();
	},
	mounted () {
		this.mdcSelect = MDCSelect.attachTo(this.$el);
		this.mdcSelectAdapter = this.mdcSelect.foundation_.adapter_;
		this.labelElement = this.mdcSelect.label_.root_;
		this.floatLabel = this.mdcSelect.label_;
		this.mdcMenu = this.mdcSelect.menu_;
		this.mdcSelect.valid = this.valid;
		this.mdcSelect.required = this.required;
		this.slotObserver = new MutationObserver(() => this.updateSlots())
		this.slotObserver.observe(this.$el, {
			childList: true,
			subtree: true
		});

		this.updateLabel();

		this.updateSlots();

		if(this.enhanced) {
			this.mdcSelect.menu_.menuSurface_.listen('MDCMenuSurface:opened', (evt) => {
				this.mdcSelect.menuElement_.style.maxWidth = this.$el.clientWidth;
				this.mdcSelect.menuElement_.style.width = this.$el.clientWidth + 'px';
			});

			this.mdcSelect.menu_.menuSurface_.listen('MDCMenuSurface:closed', (evt) => {
				this.mdcSelect.valid = this.valid;
			});
		}
	},
	beforeDestroy () {
		this.mdcSelect.destroy()
		if (typeof this.mdcNotchedOutline !== 'undefined') {
			this.mdcNotchedOutline.destroy();
		}
		this.slotObserver && this.slotObserver.disconnect();
	},
	methods: {
		getTextWidth(text, font) {
			let f = font || '12px arial',
				o = $('<div></div>')
					.text(text)
					.css({'position': 'absolute', 'float': 'left', 'white-space': 'nowrap', 'visibility': 'hidden', 'font': f})
					.appendTo($('body')),
				w = o.width();

			o.remove();

			return w;
		},
		updateLabel() {
			let showLabel = (this.value && this.float) || !this.value;
			this.value && this.$emit('change', this.selectedItem);

			this.mdcSelectAdapter.floatLabel(this.value && this.float);
			this.value && this.mdcSelectAdapter.setValue(this.value);

			let fontSize = this.labelElement.style.fontSize;
			let text = this.labelElement.innerText;
			let labelWidth = showLabel ? this.getTextWidth(text, fontSize) : 0;
			!!this.value && this.float && this.mdcSelectAdapter.notchOutline(labelWidth);
		},

		onChange (event) {
			this.$emit('change', this.getItem(event.detail.value) || event.detail);
			this.$emit('input', event.detail.value);
		},
		updateSlots () {
			if (this.enhanced && this.$slots.default) {
				this.$slots.default.map(n => {
					if (n.tag) {
						n.elm.setAttribute('role', 'option');
					}
				});
			}
		},
		getId(item) {
			return item[this.mapping.id] || item.id || item._id || '';
		},
		getText(item) {
			return item[this.mapping.text] || item.text || item.name || '';
		},
		getItem(id) {
			if(!this.hasData) {
				return null;
			}
			return this.options.find(item => this.getId(item) === id);
		}
	}
};

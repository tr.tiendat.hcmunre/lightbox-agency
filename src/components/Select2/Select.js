"use strict";

function formatItems(item) {
	if (item.loading) {
		return item.text;
	}
	var markup = "<div class='select2-result-repository clearfix'>" +
		"<div class='select2-result-item__avatar'><img src='" + require('assets/images/marker-icon.png') + "' /></div>" +
		"<div class='select2-result-item__meta'>" +
		"<div class='select2-result-item__title'>" + item.properties['display_name'] + "</div>";

	if (item.description) {
		markup += "<div class='select2-result-item__description'>" + item.description + "</div>";
	}

	markup += "<div class='select2-result-item__statistics'>" +
		"</div>" +
		"</div></div>";

	return markup;
}

function formatItemSelection(item) {
	return item.properties ? item.properties['display_name'] : item.text;
};

export default {
	name: 'Select2',
	props: ['options', 'value'],
	data: () => ({
		locationNeedReturns: [],
		ajaxSetting: {
			ajax: {
				url: "https://nominatim.openstreetmap.org/search?format=geojson&type=point",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term,
						page: params.page
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					const features = data.features.map((feature) => {
						delete feature.type;

						return {
							...feature,
							id: feature.properties.place_id
						};
					});
					return {
						results: features,
						pagination: {
							more: (params.page * 30) < data.features.length
						}
					};
				},
				cache: true
			},
			placeholder: 'Search for a location',
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 1,
			templateResult: formatItems,
			templateSelection: formatItemSelection,
		}
	}),
	method: {},
	mounted() {
		let child = this;
		const {ajaxSetting} = this;
		$(this.$el)
			.select2({
				data: this.options,
				...ajaxSetting
			}).val(this.value)
			.trigger('change')
			.on('select2:select', function (e) {
				child.$emit('input', this.value);
				child.$emit('changed', { ...e.params, value: this.value });
			});
	},
	watch: {
		value: function (value) {
			// update value
			$(this.$el)
				.val(value)
				.trigger('change');
		},
		options: function (options) {
			const {ajaxSetting} = this;
			// update options
			$(this.$el).empty().select2({
				data: options,
				...ajaxSetting
			});
		}
	},
	destroyed() {
		$(this.$el).off().select2('destroy');
	},
	updated() {
	}
};

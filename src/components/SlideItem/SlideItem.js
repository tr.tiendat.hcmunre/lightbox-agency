export default {
	name: 'slide-item',
	data: () => ({
		// isOpen: false
	}),
	props: {
		type: {
			type: String,
			default: 'item'
		},
		isHeader: {
			type: Boolean,
			default: false
		},
		icon: {
			type: String,
			default: ''
		},
		name: {
			type: String
		},
		badge: {
			type: Object,
			default () {
				return {};
			}
		},
		items: {
			type: Array,
			default () {
				return [];
			}
		},
		router: {
			type: Object,
			default () {
				return {
					name: ''
				};
			}
		},
		link: {
			type: String,
			default: ''
		}
	},
	created () {

	},
	computed: {
		getType () {
			if (this.isHeader) {
				return 'header';
			}
			return this.type === 'item' ? '' : `treeview ${this.isOpen ? 'menu-open' : ''}`;
		},
		isOpen() {
			return this.items.some(item => item.router.name === this.$route.name);
		}
	}
};

"use strict";
import { MDCSwitch } from '@material/switch';

export default {
	name: 'm-switch',
	model: {
		prop: 'checked',
		event: 'change'
	},
	props: {
		id: {
			default: ''
		},
		value: '',
		checked: {
			default: false
		},
		disabled: {
			default: false
		}
	},
	data: () => ({
		mdcSwitch: undefined
	}),
	computed: {
		classes() {
			return {
				'mdc-switch--disabled': this.disabled
			};
		},
		model: {
			get () {
				return this.checked;
			},
			set (state) {
				console.log("=====", state);
				this.$emit('change', state);
			}
		},
		isChecked() {
			if (this.checked instanceof Array) {
				return this.checked.includes(this.value);
			} else {
				return this.checked;
			}
		}
	},
	mounted() {
		this.mdcSwitch = MDCSwitch.attachTo(this.$el)
		this.mdcSwitch.checked = this.isChecked;
		this.mdcSwitch.disabled = this.disabled;
	},
	methods: {
	},
	beforeDestroy () {
		this.mdcSwitch.destroy();
	}
};

export default {
	name: 'mc-text-field',
	model: {
		prop: 'value',
		event: 'model'
	},
	props: {
		id: {
			type: String,
			default: ''
		},
		value: {
			type: String,
			default: ''
		},
		disabled: {
			type: Boolean,
			default: false
		},
		upgraded: {
			type: Boolean,
			default: false
		},
		fullWidth: {
			type: Boolean,
			default: false
		},
		dense: {
			type: Boolean,
			default: false
		},
		focused: {
			type: Boolean,
			default: false
		},
		textarea: {
			type: Boolean,
			default: false
		},
		useNativeValidation: {
			type: Boolean,
			default: true
		},
		valid: {
			type: Boolean,
			default: true
		}
	},
	computed: {
		outlined() {
			return !this.textarea;
		},
		modelValue: {
			get() {
				return this.value;
			},
			set(value) {
				this.$emit('model', value);
			}
		}
	},
	mounted () {
	},
};

import Vue from 'vue';
import Row from './Grid/Row';
import Column from './Grid/Column';
import Checkbox from './Checkbox/Checkbox.vue';
import Switch from './Switches/Switch.vue';
import ConfirmDialog from './ConfirmDialog/ConfirmDialog.vue';
import Select from './Select/Select.vue';
import MTextField from './TextField/TextField.vue';
import Autocomplete from "./Autocomplete/Autocomplete.vue";
import ControlLabel from "./ControlLabel/ControlLabel.vue";
import FormRow from "./FormRow/FormRow.vue";
import InputNumber from "./Input/index.vue";

import Button from 'material-components-vue/dist/button';
import TextField from 'material-components-vue/dist/text-field';
import List from 'material-components-vue/dist/list';
import Linear from 'material-components-vue/dist/linear-progress';
import Menu from 'material-components-vue/dist/menu';
import TopAppBar from 'material-components-vue/dist/top-app-bar';
import Drawer from 'material-components-vue/dist/drawer';
import Icon from 'material-components-vue/dist/icon';
import Radio from 'material-components-vue/dist/radio';
import Slider from 'material-components-vue/dist/slider';
import Dialog from 'material-components-vue/dist/dialog';
import FloatingLabel from 'material-components-vue/dist/floating-label';
import LineRipple from 'material-components-vue/dist/line-ripple';
import Notched from 'material-components-vue/dist/notched-outline';
import Typography from 'material-components-vue/dist/typography';
import LayoutGrid from 'material-components-vue/dist/layout-grid';
import Fab from 'material-components-vue/dist/fab';

Vue.use(Button);
Vue.use(Fab);
Vue.use(TextField);
// Vue.use(Select);
Vue.use(Radio);
Vue.use(List);
Vue.use(Linear);
Vue.use(Slider);
Vue.use(Dialog);
Vue.use(Menu);
Vue.use(TopAppBar);
Vue.use(Drawer);
Vue.use(Icon);
Vue.use(FloatingLabel);
Vue.use(LineRipple);
Vue.use(Notched);
Vue.use(Typography);
Vue.use(LayoutGrid);

// custom componets

Vue.component('row', Row);
Vue.component('column', Column);

// Vue.component('m-text-field', TextField);
Vue.component('mc-checkbox', Checkbox);
Vue.component('mc-switch', Switch);
Vue.component('mc-radio', Radio);
Vue.component('mc-dialog-confirm', ConfirmDialog);
Vue.component('mc-select', Select);
Vue.component('mc-text-field', MTextField);
Vue.component('mc-autocomplete', Autocomplete);
Vue.component('mc-control-label', ControlLabel);
Vue.component('mc-form-row', FormRow);
Vue.component('mc-input-number', InputNumber);

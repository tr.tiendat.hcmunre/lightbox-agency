import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import events from './events';
import constants from './common/const';
import Notify from './common/helpers/notify';
import ErrorHandler from './common/helpers/api/errorHandler';
import 'components';
import Vuetify from 'vuetify';
import filter from './common/filters';

Vue.use(VueAxios, axios);
Vue.use(Vuetify, {
	theme: {
		primary: '#46B38B',
		default: '#B3ADAD',
		secondary: '#B3ADAD',
		accent: '#82B1FF',
		error: '#ef4b4c',
		info: '#2196F3',
		success: '#4CAF50',
		warning: '#FFC107'
	}
});
Vue.use(filter);
Vue.events = Vue.prototype.$events = events;
Vue.const = Vue.prototype.$const = constants;
Vue.notify = Vue.prototype.$notify = Notify;
Vue.errorHandler = Vue.prototype.$errorHandler = ErrorHandler;

axios.interceptors.request.use((request) => {
	const method = request.method.toUpperCase();
	if (method === 'PUT' || method === 'DELETE' || method === 'PATCH') {
		request.headers['X-HTTP-Method-Override'] = method;
		request.method = 'post';
	}
	return request;
});

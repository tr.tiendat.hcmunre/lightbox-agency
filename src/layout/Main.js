import {mapGetters, mapActions} from "vuex";
import ContentWrap from "./ContentWrap/ContentWrap.vue";
import SideBar from "./SideBar/SideBar.vue";
import NavBar from "./NavBar/NavBar.vue";
import ControlSideBar from "./ControlSideBar/ControlSideBar.vue";
import menuItems from "./menuItems";
import {IS_IGNORE_COMMAND} from "../common/const/development.debug";
import {APP_TITLE} from "../common/configs";

export default {
	name: "Main",
	data() {
		return {
			menuItems: menuItems
		};
	},
	computed: {
		...mapGetters(["skin", "currentUser"])
	},
	components: {
		"content-wrap": ContentWrap,
		"nav-bar": NavBar,
		sidebar: SideBar,
		"control-sidebar": ControlSideBar
	},
	watch: {
		skin(val) {
			this.updateUI();
			this.skin = val;
		}
	},
	mounted() {
		if (!IS_IGNORE_COMMAND && !this.currentUser._id) {
			return this.$router.push({ name:"Login" });
		}

		$('.sidebar-menu').each(function () {
			$.fn.tree.call($(this));
		});

		this.$events.$on(this.$const.SET_TITLE, title => {
			document.title = title
				? `${title} • ${APP_TITLE}`
				: APP_TITLE;
		});

		this.$events.$emit(this.$const.SET_TITLE, "Dashboard");

		this.$nextTick(() => {
			this.updateUI();
		});
	},
	updated() {
	},
	methods: {
		...mapActions([
			// Mount action "setNumberToRemoteValue" to `this.setNumberToRemoteValue()`.
			"setSkin"
		]),
		updateUI() {
			$("#foo").alterClass("skin-* bar-*", this.skin);
		}
	}
};

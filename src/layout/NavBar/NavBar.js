import { mapGetters, mapActions } from 'vuex';
import { GETTERS, ACTIONS } from '@/store';
import MenuItem from 'components/MenuItem/MenuItem.vue';
import './NavBar.scss';

export default {
	name: 'nav-bar',
	props: {
		menuItems: {
			type: Array,
			default: () => []
		}
	},
	components: {
		'menu-item': MenuItem,
	},
	data() {
		return {
			isVisible: false,
			usagePoints: 0,
			isMenuOpen: false,
			isMenuMobileOpen: false
		};
	},
	created() { },
	computed: {
		...mapGetters([
			GETTERS.UNREAD_MSG_COUNT,
			GETTERS.UNREAD_NOTICE_COUNT,
			GETTERS.CURRENT_USER,
			GETTERS.AVATAR
		])
	},
	mounted() {
		this.setDataUser();
		let statusEl = this.$el.querySelector('.mdc-top-app-bar__status-item');
		this.$refs.mMenu.$el.style.width = statusEl.clientWidth + 'px';
		this.$refs.mMenu.$el.style.top = '60px';
	},
	methods: {
		...mapActions([ACTIONS.SIGNOUT]),
		logout() {
			this.signout().then(res => {
				if (res.errors) {
					return this.$errorHandler.showServerErrors(res.errors, this);
				}

				this.$router.push({ name: 'Login' });
			});
		},
		routeToProfile() {
			this.$router.push({ name: 'Profile' });
		},
		getAvatar() {
			return this.avatar;
		},
		setDataUser() {
			this.remainningPoints = this.currentUser.balance;
			this.user = this.currentUser.name;
		}
	}
};

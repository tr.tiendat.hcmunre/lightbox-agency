export default [
	{
		icon: "view_module",
		name: "Dashboard",
		router: {
			name: "Home"
		}
	},
	{
		type: "tree",
		icon: "supervisor_account",
		name: "Client management",
		router: {
			name: "Clients-Management"
		},
		items: [
			{
				name: 'Add Client',
				router: {
					name: "Clients-Add"
				}
			},
			{
				name: 'Edit Client',
				router: {
					name: "Clients-Edit"
				}
			},
		]
	},
	{
		type: "tree",
		icon: "queue_play_next",
		name: "Campaign management",
		router: {
			name: "Ads-Management"
		},
		items: [
			{
				name: 'Add Campaign',
				router: {
					name: "Ads-Add"
				}
			},
			{
				name: 'Edit Campaign',
				router: {
					name: "Ads-Edit"
				}
			},
		]
	},
];

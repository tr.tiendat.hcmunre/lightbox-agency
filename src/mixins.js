import Vue from 'vue';
import {setLocale} from "assets/locale/utils";

Vue.mixin({
	computed: {
		localed() {
			return setLocale;
		}
	},
	created() {
		this.$main = this.$root.$children[0];
	},
	methods: {
		hasSlots(name = 'default') {
			return this.$slots[name] && this.$slots[name].length;
		},

		uid(id) {
			return `${this._uid}_${id}`; // eslint-disable-line no-underscore-dangle
		},

		noop() {
			// noop for stopping propagation
		},
		hasErrors(prop, scope = '') {
			return this.errors.has(`${scope ? scope + '.' : ''}${prop}`);
		},
		showError(field, classes = null) {
			return this.errors.has(field) ? classes || 'has-error' : '';
		}
	},
	mounted() {
	}
});

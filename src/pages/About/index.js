import About from './About.vue';

export default {
	data: () => ({
		a: ''
	}),
	name: 'About',
	render: h => h(About),
	updated() {
		this.$event.$emit('setComTitle', 'Home');
	}
};

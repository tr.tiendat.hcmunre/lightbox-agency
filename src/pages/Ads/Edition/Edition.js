"use strict";
import {mapActions, mapGetters} from 'vuex';
import {ACTIONS} from '../../../store/modules/ad/const';
import {ACTIONS as CLIENT_ACTIONS} from '../../../store/modules/client/const';
import {GETTERS} from "../../../store";

import Client from './StepItems/Client.vue';
import Campaign from './StepItems/Campaign.vue';
import Media from './StepItems/Media.vue';
import Map from '../../../components/Map/Map.vue';
import moment from 'moment';

const enumActive = {
	INACTIVE: 'inactive',
	ACTIVE: 'active'
};

export default {
	components: {
		client: Client,
		campaign: Campaign,
		media: Media,
		Map
	},
	mixin: [],
	data: (vm) => ({
		loading: false,
		options: [
			{id: 'active', text: 'Active'},
			{id: 'inactive', text: 'Inactive'}
		],
		clientOptions: [
			{
				id: '',
				text: 'Choose a client'
			}
		],
		nearByOptions: [
			{
				id: 'school',
				text: 'School'
			},
			{
				id: 'hotel',
				text: 'Hotel'
			},
			{
				id: 'hospital',
				text: 'Hospital'
			}
		],
		loc: {
			cities: [
			],
			districts: [],
		},
		formData: {
			campaign_id: '',
			campaign_name: '',
			clientInfo: {
			},
			campaignInfo: {
			},
			media: {
			}
		},
		currentStep: 1,
		hasFile: false,
		isSubmitting: false,
		city: null,
		isConfirm: false,
		confirmMessage: 'All your changes will be lost. Are you sure?'
	}),
	computed: {
		...mapGetters({
			appLoad: GETTERS.LOADING,
			currentUser: GETTERS.CURRENT_USER
		})
	},
	watch: {
		async $route(to, from) {
			if (to.path !== from.path) {
				this.reload();
			}
		},
		city() {
			this.initDistrict();
		},
		loading(val) {
			this.loaded = !val;
		}
	},
	updated() {
	},
	async created() {
		await this.loadMaster();
		this.reload();
		const dict = {
			custom: {
				name: this.localed.name,
				file: this.localed.file,
				times: this.localed.datetime,
				available: this.localed.available,
				location: this.localed.location
			}
		};

		this.$validator.localize('en', dict);
	},
	mounted() {
	},
	methods: {
		...mapActions({
			loadCampaign: ACTIONS.LOAD,
			editAd: ACTIONS.EDIT_AD,
			loadClients: CLIENT_ACTIONS.LOAD_ALL,
			loadDevices: ACTIONS.LOAD_DEVICES
		}),
		reset() {
			this.formData = {
				client_id: null,
				name: null,
				file: null,
				media_file: null,
				status: enumActive.ACTIVE,
				description: null,
				attributes: {
					nearby: null,
					location: {
						city: null,
						districts: []
					},
					times: {
						from: null,
						to: null
					},
				},
				available: {
					from: null,
					to: null
				}
			};
		},
		async loadMaster() {},
		processCampaign(campaign) {
			this.formData.campaign_id = campaign._id;
			this.formData.campaign_name = campaign.name;
			this.formData.clientInfo = {
				name: campaign.name,
				client: campaign.client_id,
				description: campaign.description
			};
			this.formData.campaignInfo = {
				districts: campaign.attributes.location.districts,
				city: campaign.attributes.location.city,
				available: campaign.available,
				nears_by: campaign.attributes.nearby,
				times: campaign.attributes.times,
				budget: campaign.attributes.budget
			};
		},
		reload() {
			this.reset();
			if (this.$route.params.id) {
				this.loading = true;
				this.loadCampaign(this.$route.params.id).then(res => {
					this.loading = false;

					if(res.errors) {
						this.showNotify('error', 'Load campaign failed!');
						return;
					}

					this.processCampaign(res.ad);
				}).catch(err => {
					console.log('Errors load ad: ', err);
					this.loading = false;
					return false;
				});
			}
		},

		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		showError(field) {
			return this.errors.has(field) ? 'has-error' : '';
		},
		validateForm() {
			this.$validator.validateAll().then((result) => {
				if (result) {
					if (!this.isSubmitting) {
						this.isSubmitting = true;
						this.editAd(this.formData).then(res => {
							if (res.errors) {
								return this.$errorHandler.showServerErrors(res.errors, this);
							}
							this.$notify.success('Save ad successfuly');
							return this.$router.push({name: "Ads-Management"});
						});
					}
				}
			});

		},
		onCancel(evt) {
			this.isConfirm = true;
		},
		onSaved(campaign) {
			this.processCampaign(campaign);
		},
		doConfirm() {
			this.isConfirm = false;
			this.$router.push({name: 'Ads-Management'});
		},
		closeConfirm() {
			this.isConfirm = false;
		}
	}
};

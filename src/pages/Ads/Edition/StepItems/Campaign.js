import {mapActions} from "vuex";
import {ACTIONS as AD_ACTIONS} from "@/store/modules/ad/const";
import Input from 'components/Input/index.vue';
import Audience from '../../Item/Audience/index.vue';
import Environment from '../../Item/Environment/index.vue';
import moment from "moment";
import { commonMixin, dateTimeMixin } from '@/common/mixins';
import Map from 'components/Map/Map.vue';

export default {
	name: 'campaign',
	components: {
		Input,
		Audience,
		Environment,
		Map
	},
	props: {
		campaign_id: {
			type: String,
			default: ''
		},
		campaign_name: {
			type: String,
			default: ''
		},
		campaignInfo: {
			type: Object,
			default: () => ({})
		}
	},
	mixins: [commonMixin, dateTimeMixin],
	data: (vm) => ({
		cities: [],
		districts: [],
		nears_by: [
			{
				id: 'school',
				name: 'School'
			},
			{
				id: 'hospital',
				name: 'Hospital'
			},
			{
				id: 'market',
				name: 'Market'
			}
		],
		campaign: {
			city: '',
			districts: '',
			nearby: '',
			available: {
				from: moment(new Date().toISOString().substr(0, 10)).unix(),
				to: moment(new Date().toISOString().substr(0, 10)).unix(),
			},
			times: {
				from: vm.stringToTime(moment().format('HH:mm')),
				to: vm.stringToTime(moment().format('HH:mm')),
			},
			budget: 50,
			costInfo: {
				devices: 0,
				viewer: 0,
				impressions: 0,
				totalCost: 0
			}
		},
		minDate: moment().add(1, 'day').format('YYYY-MM-DD'),
		isShowStartDate: false,
		isShowEndDate: false,
		isShowEndTimeDialog: false,
		isShowStartTimeDialog: false,
		mapMarkers: [
		],
		startDateTime: null,
		endDateTime: null,
		mapCenter: [],
	}),
	async created() {
		const dict = {
			custom: {
				fromDate: this.localed.fromDate,
				fromTime: this.localed.times,
				budget: this.localed.budget
			}
		};
		this.$validator.localize('en', dict);
	},
	async mounted() {
		if(this.campaignInfo.city) {
			await this.getDistricts(this.campaignInfo.city);
		}
		await this.getCities();
		await this.getDevices();

		this.processCampaign(this.campaignInfo);
		this.onCalculateCost();
	},
	methods: {
		...mapActions({
			loadCities: AD_ACTIONS.LOAD_CITY,
			loadDistricts: AD_ACTIONS.LOAD_DISTRICT,
			loadDevices: AD_ACTIONS.LOAD_DEVICES,
			editCampaign: AD_ACTIONS.EDIT_AD,
		}),
		processCampaign(campaign) {
			this.campaign.city = campaign.city;
			this.campaign.districts = campaign.districts;
			this.campaign.nearby = campaign.nears_by;
			this.campaign.available = campaign.available;
			this.campaign.times = campaign.times;
			this.campaign.budget = campaign.budget;
			this.startDateTime = +moment.utc(campaign.available.from).add(campaign.times.from, 'minute');
			this.endDateTime = +moment.utc(campaign.available.to).add(campaign.times.to, 'minute');
		},
		getDistrictText(id) {
			let district = this.districts.find(item => item._id === id) || {};
			return district.name || id;
		},
		getNearByText(id) {
			let item = this.nears_by.find(item => item.id === id) || {};
			return item.name || id;
		},
		getCities() {
			return this.loadCities().then(res => {
				if(res.errors) {
					console.log(res.errors);
					return;
				}
				this.cities = res.cities;
			});
		},
		getDevices() {
			return this.loadDevices().then(res => {
				if(res.errors) {
					console.log(res.errors);
					return;
				}
				this.mapMarkers = res.devices.map(item => {
					return {
						position: [item.location.coordinates[1], item.location.coordinates[0]],
					};
				});
			});
		},
		async getDistricts(city) {
			return this.loadDistricts({city}).then(res => {
				if(res.errors) {
					console.log(res.errors);
					return;
				}

				this.districts = res.districts || [];
			});
		},
		async onChangeCity(evt) {
			if(evt.central) {
				let center = evt.central.coordinates;
				this.mapCenter = [center[1], center[0]];
			}

			if (this.campaign.city === evt._id) return;
			this.onCalculateCost();
			this.campaign.districts = []; // reset
			if(evt._id) {
				this.getDistricts(evt._id);
			}
		},
		onCalculateCost() {
			const costInfo = {
				devices: this.mapMarkers.length,
				viewer: this.formatNumber(this.randomNumber(1000, 5000)),
				impressions: this.formatNumber(this.randomNumber(10, 500)),
				totalCost: this.formatNumber(this.randomNumber(50, 1000))
			};
			this.campaign.costInfo = costInfo;
		},
		onChangeDistricts(evt) {
			this.onCalculateCost();
		},
		onChangeNearby(evt) {
			this.onCalculateCost();
		},
		onChangeStartDate(evt) {
			this.isShowStartDate = false;
			this.onCalculateCost();
			this.campaign.available.from = this.stringToDate(evt, 'YYYY-MM-DD');
			this.startDateTime = +moment.utc(this.campaign.available.from).add(this.campaign.times.from, 'minute');

		},
		onChangeEndDate(evt) {
			this.isShowEndDate = false;
			this.campaign.available.to = this.stringToDate(evt, 'YYYY-MM-DD');
			this.endDateTime = +moment.utc(this.campaign.available.to).add(this.campaign.times.to, 'minute');
			this.onCalculateCost();
		},
		onChangeStartTime(evt) {
			this.campaign.times.from = this.stringToTime(evt);
			this.startDateTime = +moment.utc(this.campaign.available.from).add(this.campaign.times.from, 'minute');
			this.onCalculateCost();
		},
		onChangeEndTime(evt) {
			this.campaign.times.to = this.stringToTime(evt);
			this.endDateTime = +moment.utc(this.campaign.available.to).add(this.campaign.times.to, 'minute');
			this.onCalculateCost();
		},
		getPostData() {
			return {
				_id: this.campaign_id,
				name: this.campaign_name,
				attributes: {
					times: this.campaign.times,
					location: {
						city: this.campaign.city,
						districts: this.campaign.districts.map(item => item._id || item),
					},
					nearby: this.campaign.nearby.map(item => item.id || item),
					budget: this.campaign.budget
				},
				available: this.campaign.available
			};
		},
		save() {
			this.$validator.validateAll().then((isValid) => {
				if (isValid) {
					this.editCampaign(this.getPostData()).then(res => {
						if (res.errors) {
							return this.$errorHandler.showServerErrors(res.errors, this);
						}
						this.$notify.success('Save Campaign successfully');
					});
				}
			});
		},
	}
};

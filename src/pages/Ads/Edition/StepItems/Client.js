import {mapActions} from "vuex";
import {ACTIONS as CLIENT_ACTIONS} from "@/store/modules/client/const";
import {ACTIONS} from "@/store/modules/ad/const";

export default {
	name: 'client',
	props: {
		campaign_id: {
			type: String,
			default: ''
		},
		clientInfo: {
			type: Object,
			default: () => ({})
		}
	},
	computed: {
	},
	data: () => ({
		clientSelected: {},
		clientList: [],
		client: {}
	}),
	watch:{
		clientInfo(){
			this.client = Object.assign({}, this.clientInfo);
		}
	},
	created() {
		this.getClients();
		const dict = {
			custom: {
				name: this.localed.name,
				client: this.localed.client
			}
		};
		this.$validator.localize('en', dict);
	},
	mounted() {
		this.client = Object.assign({}, this.clientInfo);
	},
	methods: {
		...mapActions({
			apiGetClientList: CLIENT_ACTIONS.LOAD_ALL,
			editCampaign: ACTIONS.EDIT_AD,
		}),
		onChangeClient(client) {
			this.clientSelected = client;
		},
		cancel(evt) {},
		save() {
			this.$validator.validateAll().then(async (isValid) => {
				if (isValid) {
					this.editCampaign(this.getPostData()).then(res => {
						if (res.errors) {
							return this.$errorHandler.showServerErrors(res.errors, this);
						}

						this.client.name = res.client.name;
						// this.$emit('saved', res.client);
						return this.$notify.success('Save Campaign successfully');
					});
				}
			});
		},
		getClients() {
			return this.apiGetClientList().then(res => {
				this.clientList = res.clients.data;
			});
		},
		getPostData() {
			return {
				_id: this.campaign_id,
				name: this.client.name,
				client_id: this.client.client,
				description: this.client.description
			};
		}
	}
};

import {ACTIONS as AD_ACTIONS} from '@/store/modules/ad/const';
import {mapActions, mapGetters} from "vuex";
import {GETTERS} from "@/store";
import {BASE_API_URL} from "@/common/configs/api";

const enumAction = {
	DELETE_MEDIA: 'DELETE_MEDIA'
};

const enumVideoState = {
	UNLOAD: 0,
	LOADING: 1,
	LOADED: 2
};

export default {
	name: 'media',
	props: {
		campaign_id: {
			type: String,
			default: ''
		},
		campaign_name: {
			type: String,
			default: ''
		},
		Media: {
			type: Object,
			default: () => ({})
		}
	},
	data(vm) {
		console.log('campaign_id: ', vm.$props.campaign_id);
		console.log('Media: ', vm.$props.Media);
		return {
			videoElement: null,
			paused: null,
			videoState: enumVideoState.UNLOAD,
			isConfirmAction: false,
			dialogMessage: '',
			action: '',
			media: {
				name: '',
				url: '',
				file: '',
				type: '',
			},
		};
	},
	computed: {
		...mapGetters({
			currentUser: GETTERS.CURRENT_USER,
		}),
	},
	async created() {
		const campaign = await this.loadCampaign();
		this.loadMedia(campaign);
	},
	methods: {
		...mapActions({
			apiLoadCampaign: AD_ACTIONS.LOAD,
			editCampaign: AD_ACTIONS.EDIT_AD,
		}),
		updatePaused(evt) {
			this.videoElement = evt.target;
			this.paused = evt.target.paused;
		},
		play() {
			this.videoElement.play();
		},
		pause() {
			this.videoElement.pause();
		},
		onConfirmDeleteMedia(mediaName, evt) {
			this.isConfirmAction = true;
			this.action = enumAction.DELETE_MEDIA;
			this.dialogMessage = `Are you sure you want to delete ${mediaName}?`;
		},
		onDeleteMedia() {
			this.isConfirmAction = false;
			this.cleanMedia();
			this.$refs.mediaUpload.value = null;
		},
		onHandlePickMedia() {
			this.$refs.mediaUpload.click();
		},
		cleanMedia() {
			this.media = {
				name: '',
				file: '',
				url: '',
				type: ''
			};
			this.videoState = enumVideoState.UNLOAD;
		},
		onFilePicked(evt) {
			const files = evt.target.files;

			if (!files.length) {
				return;
			}

			this.cleanMedia();
			this.videoState = enumVideoState.LOADING;

			if (files[0] !== undefined) {
				this.media.name = files[0].name;
				this.media.type = this.checkMediaType(files[0].type);
				if (this.media.name.lastIndexOf('.') <= 0) {
					return;
				}
				const fr = new FileReader();
				fr.readAsDataURL(files[0]);
				fr.addEventListener('load', () => {
					this.media.url = fr.result;
					this.media.file = files[0];
					this.videoState = enumVideoState.LOADED;
				});
			} else {
				this.cleanMedia();
			}
		},
		checkMediaType(type) {
			return type.split('/')[0];
		},
		showError(field) {
			return this.errors.has(field) ? 'has-error' : '';
		},
		onDoAction(action) {
			switch (action) {

			case enumAction.DELETE_MEDIA:
				return this.onDeleteMedia();

			default:
				break;
			}
		},
		onNoAction() {
			this.isConfirmAction = false;
			this.action = null;
			this.dialogMessage = null;
		},
		loadCampaign() {
			const id = this.campaign_id;
			if (id) {
				return this.apiLoadCampaign(id).then(response => {
					return response.ad;
				});
			}
			return {};
		},
		loadMedia(campaign) {
			const media = campaign.file;
			this.setMediaFile(media);
		},
		setMediaFile(media) {
			if (!media) return;
			const {file_name: name, file_type: type} = media;
			const idUser = this.currentUser._id;
			this.videoState = enumVideoState.LOADED;
			this.media = {
				name,
				file: true,
				url: `${BASE_API_URL}/user/${idUser}/avatar/${name}`,
				type: this.checkMediaType(type)
			};
		},
		nextStep() {
			this.$validator.validateAll().then((isValid) => {
				if (isValid) {
					this.editCampaign(this.getPostData()).then(res => {
						if (res.errors) {
							return this.$errorHandler.showServerErrors(res.errors, this);
						}
						this.$notify.success('Save Campaign successfully');
					});
				}
			});
		},
		getPostData() {
			return {
				_id: this.campaign_id,
				name: this.campaign_name,
				file: this.media.file
			};
		}
	}
};

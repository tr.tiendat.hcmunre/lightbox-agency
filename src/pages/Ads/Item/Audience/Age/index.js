import Collapse from '@/components/Collapse/index.vue';

export default {
	name: 'Age',
	data: () => ({
		seasons: [
			'18-24',
			'25-34',
			'35-44',
			'45-54',
			'55-64'
		],
	}),
	components: {
		Collapse
	},
	methods: {
		season (val) {
			return this.seasons[val];
		},
	}
};

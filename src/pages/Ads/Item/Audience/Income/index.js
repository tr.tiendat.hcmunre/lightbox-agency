import Collapse from '@/components/Collapse/index.vue';

export default {
	name: 'Income',
	data: () => ({
		seasons: [
			'<$25k',
			'$25k-$39,9k',
			'$40k-$59,9k',
			'$60k-$74,9k',
			'<$75k',
		],
	}),
	components: {
		Collapse
	},
	methods: {
		season (val) {
			return this.seasons[val];
		},
	}
};

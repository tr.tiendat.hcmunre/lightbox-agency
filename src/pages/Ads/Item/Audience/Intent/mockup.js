export const items = [{
	"id": 10001,
	"name": "Auto Shoppers",
	"type": "Automotive & Boating",
}, {
	"id": 20001,
	"name": "Ticket Shoppers",
	"type": "Events",
}, {
	"id": 30001,
	"name": "Designer Shoppers",
	"type": "Fashion",
}, {
	"id": 40001,
	"name": "Home Shoppers",
	"type": "Real Estate",
}, {
	"id": 50001,
	"name": "Auction Shoppers",
	"type": "Retail",
}, {
	"id": 50002,
	"name": "Bargain Shoppers",
	"type": "Retail",
}, {
	"id": 50003,
	"name": "Coupon Clippers",
	"type": "Retail",
}, {
	"id": 50004,
	"name": "Daily Deals Consumers",
	"type": "Retail",
}, {
	"id": 50005,
	"name": "Flash Sales Consumers",
	"type": "Retail",
}, {
	"id": 50006,
	"name": "Gift Givers",
	"type": "Retail",
}, {
	"id": 50007,
	"name": "Impulse Buyers",
	"type": "Retail",
}, {
	"id": 60001,
	"name": "Tech & Gadget Shoppers",
	"type": "Technology",
}, {
	"id": 70001,
	"name": "Flight Shoppers",
	"type": "Travel",
}, {
	"id": 70002,
	"name": "Hotel Shoppers",
	"type": "Travel",
}, {
	"id": 70003,
	"name": "Travel Shoppers",
	"type": "Travel",
}, {
	"id": 70004,
	"name": "Vacation Shoppers",
	"type": "Travel",
}];

export const items = [{
	"id": 10001,
	"name": "Arts and Crafts Lovers",
	"type": "Arts & Entertainment",
}, {
	"id": 10002,
	"name": "Avid Readers",
	"type": "Arts & Entertainment",
}, {
	"id": 10003,
	"name": "Concertgoers",
	"type": "Arts & Entertainment",
}, {
	"id": 10004,
	"name": "Movie Buffs",
	"type": "Arts & Entertainment",
}, {
	"id": 10005,
	"name": "Music Discovery Enthusiasts",
	"type": "Arts & Entertainment",
}, {
	"id": 10006,
	"name": "Music Lovers",
	"type": "Arts & Entertainment",
}, {
	"id": 10007,
	"name": "Music Streamers",
	"type": "Arts & Entertainment",
}, {
	"id": 10008,
	"name": "Photo & Video Enthusiasts",
	"type": "Arts & Entertainment",
}, {
	"id": 10009,
	"name": "Photo Editors",
	"type": "Arts & Entertainment",
}, {
	"id": 100010,
	"name": "Photo Sharers",
	"type": "Arts & Entertainment",
}, {
	"id": 20001,
	"name": "Vehicle Owners",
	"type": "Automotive & Boating",
}, {
	"id": 30001,
	"name": "Influencers",
	"type": "Communications",
}, {
	"id": 30002,
	"name": "Social Influencers",
	"type": "Communications",
}, {
	"id": 40001,
	"name": "Casual Investors",
	"type": "Financial Services",
}, {
	"id": 40002,
	"name": "Home Financial Planners",
	"type": "Financial Services",
}, {
	"id": 40003,
	"name": "Insurance Owners",
	"type": "Financial Services",
}, {
	"id": 40004,
	"name": "Mobile Banking Users",
	"type": "Financial Services",
}, {
	"id": 40005,
	"name": "Mobile Payment Makers",
	"type": "Financial Services",
}, {
	"id": 50001,
	"name": "Coffee Connoisseurs",
	"type": "Food & Restaurants",
}, {
	"id": 50002,
	"name": "Fast Food Patrons",
	"type": "Food & Restaurants",
}, {
	"id": 50003,
	"name": "Foodies",
	"type": "Food & Restaurants",
}, {
	"id": 50004,
	"name": "Home Chefs",
	"type": "Food & Restaurants",
}, {
	"id": 50005,
	"name": "Restaurant Hoppers",
	"type": "Food & Restaurants",
}, {
	"id": 60001,
	"name": "Casino Gamers",
	"type": "Gaming",
}, {
	"id": 60001001,
	"name": "Casino App Players",
	"type": "Casino Gamers",
}, {
	"id": 60002,
	"name": "Card & Board Gamers",
	"type": "Gaming",
}, {
	"id": 60003,
	"name": "eSports Fans",
	"type": "Gaming",
}, {
	"id": 60004,
	"name": "Hardcore Gamers",
	"type": "Gaming",
}, {
	"id": 60005,
	"name": "Social & Casual Gamers",
	"type": "Gaming",
}, {
	"id": 70001,
	"name": "Avid Cyclists",
	"type": "Health & Fitness",
}, {
	"id": 70002,
	"name": "Avid Runners",
	"type": "Health & Fitness",
}, {
	"id": 70003,
	"name": "Calorie Counters",
	"type": "Health & Fitness",
}, {
	"id": 70004,
	"name": "Fitness Device Owners",
	"type": "Health & Fitness",
}, {
	"id": 70005,
	"name": "Health & Fitness Enthusiasts",
	"type": "Health & Fitness",
}, {
	"id": 80001,
	"name": "Home & Garden Pros",
	"type": "Home & Garden",
}, {
	"id": 80002,
	"name": "Home Improvement Enthusiasts",
	"type": "Home & Garden",
}, {
	"id": 90001,
	"name": "Chinese Speakers",
	"type": "Language",
}, {
	"id": 90002,
	"name": "French Speakers",
	"type": "Language",
}, {
	"id": 90003,
	"name": "German Speakers",
	"type": "Language",
}, {
	"id": 90004,
	"name": "Italian Speakers",
	"type": "Language",
}, {
	"id": 90005,
	"name": "Japanese Speakers",
	"type": "Language",
}, {
	"id": 90006,
	"name": "Spanish Speakers",
	"type": "Language",
}, {
	"id": 100001,
	"name": "e-News Readers",
	"type": "News",
}, {
	"id": 100002,
	"name": "News Readers",
	"type": "News",
}, {
	"id": 110001,
	"name": "On-Demand Economy Participants",
	"type": "On Demand Economy",
}, {
	"id": 110002,
	"name": "On-Demand Learning",
	"type": "On Demand Economy",
}, {
	"id": 110003,
	"name": "On-Demand Lodging",
	"type": "On Demand Economy",
}, {
	"id": 110004,
	"name": "On-Demand Transportation",
	"type": "On Demand Economy",
}, {
	"id": 120001,
	"name": "Political Views - Democrat",
	"type": "Politics",
}, {
	"id": 120002,
	"name": "Political Views - Republican",
	"type": "Politics",
}, {
	"id": 130001,
	"name": "Ride Sharers",
	"type": "Sharing Economy",
}, {
	"id": 130002,
	"name": "Room Sharers",
	"type": "Sharing Economy",
}, {
	"id": 130003,
	"name": "Sharing Economy",
	"type": "Sharing Economy",
}, {
	"id": 140001,
	"name": "American Football Fans",
	"type": "Sports & Recreation",
}, {
	"id": 140002,
	"name": "Baseball Fans",
	"type": "Sports & Recreation",
}, {
	"id": 140003,
	"name": "Basketball Fans",
	"type": "Sports & Recreation",
}, {
	"id": 140004,
	"name": "College Sports Fans",
	"type": "Sports & Recreation",
}, {
	"id": 140005,
	"name": "Extreme Sports Enthusiasts",
	"type": "Sports & Recreation",
}, {
	"id": 140006,
	"name": "Fantasy Sports Players",
	"type": "Sports & Recreation",
}, {
	"id": 140007,
	"name": "Hockey Fans",
	"type": "Sports & Recreation",
}, {
	"id": 140008,
	"name": "Outdoor Sports Enthusiasts",
	"type": "Sports & Recreation",
}, {
	"id": 140009,
	"name": "Sports Fans",
	"type": "Sports & Recreation",
}, {
	"id": 150001,
	"name": "Home Automation Enthusiasts",
	"type": "Technology",
}, {
	"id": 150002,
	"name": "Home Entertainment Ethusiasts",
	"type": "Technology",
}, {
	"id": 150003,
	"name": "Tech & Gadget Enthusiasts",
	"type": "Technology",
}, {
	"id": 160001,
	"name": "Wireless Plan App Owners",
	"type": "Telecommunications",
}, {
	"id": 170001,
	"name": "Commuters",
	"type": "Travel",
}, {
	"id": 170002,
	"name": "Vacationers",
	"type": "Travel",
}, {
	"id": 180001,
	"name": "Basic TV Streamers",
	"type": "Video Streaming",
}, {
	"id": 180002,
	"name": "Free Content Streamers",
	"type": "Video Streaming",
}, {
	"id": 180003,
	"name": "On Demand Movie Streamers",
	"type": "Video Streaming",
}, {
	"id": 180004,
	"name": "Over the Top Device Owners",
	"type": "Video Streaming",
}, {
	"id": 180005,
	"name": "Service Provider Streamers",
	"type": "Video Streaming",
}, {
	"id": 180006,
	"name": "Subscription Content Streamers",
	"type": "Video Streaming",
}, {
	"id": 180007,
	"name": "TV Cord Cutters",
	"type": "Video Streaming",
}, {
	"id": 180008,
	"name": "TV Subscribers",
	"type": "Video Streaming",
}, {
	"id": 180009,
	"name": "Video Streamers",
	"type": "Video Streaming",
}];

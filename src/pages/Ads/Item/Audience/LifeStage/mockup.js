export const items = [{
	"id": 10001,
	"name": "Business Professionals",
	"type": "Business",
}, {
	"id": 10002,
	"name": "Job Seekers",
	"type": "Business",
}, {
	"id": 10003,
	"name": "Small Business Owners",
	"type": "Business",
}, {
	"id": 20001,
	"name": "Students",
	"type": "Education",
}, {
	"id": 30001,
	"name": "Pet Owners",
	"type": "Family Status",
}, {
	"id": 30001001,
	"name": "Dog Owners",
	"type": "Family Status",
}, {
	"id": 30001002,
	"name": "Pet Owners",
	"type": "Family Status",
}, {
	"id": 30002,
	"name": "Parents",
	"type": "Family Status",
}, {
	"id": 40001,
	"name": "Singles",
	"type": "Marital Status",
}, {
	"id": 50001,
	"name": "Apartment Renters",
	"type": "Real Estate",
}, {
	"id": 50002,
	"name": "Home Owners",
	"type": "Real Estate",
}, {
	"id": 60001,
	"name": "New Immigrants",
	"type": "New Immigrants",
}];

import Age from './Age/index.vue';
import Income from './Income/index.vue';
import InterestAndActivity from './InterestAndActivity/index.vue';
import LifeStage from './LifeStage/index.vue';
import Intent from './Intent/index.vue';

export default {
	name: 'Audience',
	data: () => ({
	}),
	components: {
		Age,
		Income,
		InterestAndActivity,
		LifeStage,
		Intent
	},
	methods: {

	}
};

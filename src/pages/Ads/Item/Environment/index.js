import Collapse from '@/components/Collapse/index.vue';
import NTreeview from '@/components/NTreeview/index.vue';
import { items as apiItems } from './mockup';
export default {
	name: 'Environment',
	data: () => ({
		items: [],
		selectedItems: []
	}),
	async created() {
		this.items = await this.fetch();
	},
	components: {
		Collapse,
		NTreeview,
	},
	watch: {
		selectedItems (val) {
			console.log('selected Items: ', this.selectedItems);
		}
	},
	methods: {
		fetch () {
			if (this.items.length) return;

			return new Promise(resolve => setTimeout(() => {
				resolve(apiItems);
			}, 800));
		},
	}
};

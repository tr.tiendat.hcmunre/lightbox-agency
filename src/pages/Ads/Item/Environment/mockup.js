export const items = [{
	"id": 10001,
	"name": "Washroom",
	"type": "Restobar",
}, {
	"id": 10002,
	"name": "Unspecified",
	"type": "Restobar",
}, {
	"id": 10003,
	"name": "Hallway",
	"type": "Restobar",
}, {
	"id": 20001,
	"name": "Elevator",
	"type": "Office",
}, {
	"id": 20002,
	"name": "Lobby",
	"type": "Office",
}, {
	"id": 20003,
	"name": "Food Court",
	"type": "Office",
}, {
	"id": 20004,
	"name": "Unspecified",
	"type": "Office",
}, {
	"id": 30001,
	"name": "Elevator",
	"type": "Residential",
}, {
	"id": 30002,
	"name": "Lobby",
	"type": "Residential",
}, {
	"id": 30003,
	"name": "Unspecified",
	"type": "Residential",
}, {
	"id": 40001,
	"name": "Convenience Store",
	"type": "Convenience Store",
}, {
	"id": 50001,
	"name": "Grocery Store",
	"type": "Grocery Store",
}, {
	"id": 60001,
	"name": "Clinic Waiting Room",
	"type": "Health",
}, {
	"id": 60002,
	"name": "Pharmacy",
	"type": "Health",
}, {
	"id": 70001,
	"name": "Lobby",
	"type": "Sports & Recreational Centers",
}, {
	"id": 70002,
	"name": "Snack Bar",
	"type": "Sports & Recreational Centers",
}, {
	"id": 70003,
	"name": "Viewing Gallery",
	"type": "Sports & Recreational Centers",
}, {
	"id": 70004,
	"name": "Gym",
	"type": "Sports & Recreational Centers",
}, {
	"id": 70005,
	"name": "Washroom",
	"type": "Sports & Recreational Centers",
}, {
	"id": 70006,
	"name": "Changing Room",
	"type": "Sports & Recreational Centers",
}, {
	"id": 70007,
	"name": "Unspecified",
	"type": "Sports & Recreational Centers",
}, {
	"id": 80001,
	"name": "Subway & LRT",
	"type": "Subway & LRT",
}, {
	"id": 90001,
	"name": "Hallway",
	"type": "Airport",
}, {
	"id": 90002,
	"name": "Gate",
	"type": "Airport",
}, {
	"id": 100001,
	"name": "Malls",
	"type": "Malls",
}];

import {mapActions} from "vuex";
import {ACTIONS as CLIENT_ACTIONS} from "@/store/modules/client/const";
import {ACTIONS as AD_ACTIONS} from "@/store/modules/ad/const";
import './styles.scss';
import Map from '../../../components/Map/Map.vue';
import Input from '../../../components/Input/index.vue';
import moment from 'moment';
import Audience from './Audience/index.vue';
import Environment from './Environment/index.vue';

const enumStep = {
	CLIENT_INFORMATION: 1,
	CAMPAIGN_INFORMATION: 2,
	MEDIA: 3
};

const enumAction = {
	CANCEL_CREATE: 'CANCEL_CREATE',
	DELETE_MEDIA: 'DELETE_MEDIA'
};

const enumActive = {
	ACTIVE: 'active',
	INACTIVE: 'inactive'
};

const enumVideoState = {
	UNLOAD: 0,
	LOADING: 1,
	LOADED: 2
};

const defaultDate = moment.utc().add(1, 'days');
const getDefaultTime = () => {
	var m = defaultDate;
	m.utcOffset(0);
	m.set({hour: 0, minute: 0, second: 0, millisecond: 0});
	m.toISOString();
	return m.format('HH:mm');
};
const defaultTime = getDefaultTime();

export default {
	name: 'Ad',
	mixin: [],
	data(vm) {
		return {
			videoState: enumVideoState.UNLOAD,
			isConfirmAction: false,
			dialogMessage: '',
			action: '',
			videoElement: null,
			paused: null,
			cities: [],
			districts: [],
			nearbys: [{id: 'school', name: 'School'}, {id: 'hospital', name: 'Hospital'}, {id: 'market', name: 'Market'}],
			campaign: {
				clientInfo: {name: '', client: '', description: ''},
				campaignInfo: {
					city: '',
					district: '',
					nearby: '',
					available: {
						from: defaultDate.valueOf(),
						to: defaultDate.valueOf(),
					},
					times: {
						from: vm.setTime(defaultTime),
						to: vm.setTime(defaultTime),
					},
					budget: 50,
					costInfo: {devices: 0, viewer: 0, impressions: 0, totalCost: 0}
				},
				media: {name: '', url: '', file: '', type: ''}
			},
			clientList: [],
			clientListFromApi: [],
			clientSelected: {},
			currentStep: enumStep.CLIENT_INFORMATION,
			minStep: 1,
			maxStep: 3,

			fromDateFormat: defaultDate.toISOString().substr(0, 10),
			fromDateFormatted: vm.formatDate(defaultDate.toISOString().substr(0, 10)),
			toDateFormat: defaultDate.toISOString().substr(0, 10),
			toDateFormatted: vm.formatDate(defaultDate.toISOString().substr(0, 10)),

			fromTimeFormat: defaultTime,
			toTimeFormat: defaultTime,

			startDateTime: +moment.utc().startOf('day'),
			endDateTime: +moment.utc().startOf('day'),
			minDate: moment.utc().add(1, 'day').format('YYYY-MM-DD'),
			isShowStartDate: false,
			isShowEndDate: false,
			isShowEndTimeDialog: false,
			isShowStartTimeDialog: false,
			mapMarkers: [{position: [43.7170226, -79.4197830350134], text: 11}, {position: [43.7270226, -79.4197830350134], text: 12}],
			mapCenter: [],
		};
	},
	computed: {
		isPlaying() {
			return !this.paused;
		},
		computedStartDateFormatted() {
			return this.formatDate(this.fromDateFormat);
		},
		computedEndDateFormatted() {
			return this.formatDate(this.toDateFormat);
		},
	},
	watch: {
		fromDateFormat(val) {
			const timeStampByDate = +moment.utc(val);
			this.fromDateFormatted = this.formatDate(this.fromDateFormat);
			this.campaign.campaignInfo.available.from = timeStampByDate;
			this.onCalculateCost();
		},
		toDateFormat(val) {
			const timeStampByDate = +moment.utc(val);
			this.toDateFormatted = this.formatDate(this.toDateFormat);
			this.campaign.campaignInfo.available.to = timeStampByDate;
			this.onCalculateCost();
		},
		'campaign.campaignInfo.budget'(val) {
			this.onCalculateCost();
		}
	},
	components: {
		Map,
		Input,
		Audience,
		Environment,
	},
	created() {
		const dict = {
			custom: {
				fieldName: this.localed.name,
				fieldClients: this.localed.client,
				budget: this.localed.budget,
				fromDate: this.localed.fromDate,
				fromTime: this.localed.fromTime,
			}
		};
		this.$validator.localize('en', dict);

		this.getCities();
		this.getDevices();
	},
	mounted() {
		this.setClientList();
		this.onCalculateCost();
	},
	filters: {
		timeToString: (value) => {
			if (!value) return '';
			let h = Math.ceil(value / 60);
			let m = value % 60;
			return `${h}:${m}`;
		}
	},
	updated() {
	},
	methods: {
		updatePaused(evt) {
			this.videoElement = evt.target;
			this.paused = evt.target.paused;
		},
		play() {
			this.videoElement.play();
		},
		pause() {
			this.videoElement.pause();
		},
		...mapActions({
			apiGetClientList: CLIENT_ACTIONS.LOAD_ALL,
			editAd: AD_ACTIONS.EDIT_AD,
			loadCities: AD_ACTIONS.LOAD_CITY,
			loadDistricts: AD_ACTIONS.LOAD_DISTRICT,
			loadDevices: AD_ACTIONS.LOAD_DEVICES
		}),
		setTime(value) {
			const timeToNumberArray = value.split(':');
			return timeToNumberArray[0] * 60 + parseInt(timeToNumberArray[1]);
		},
		onConfirmDeleteMedia(mediaName, evt) {
			this.isConfirmAction = true;
			this.action = enumAction.DELETE_MEDIA;
			this.dialogMessage = `Are you sure you want to delete ${mediaName}?`;
		},
		onDeleteMedia() {
			this.isConfirmAction = false;
			this.campaign.media.name = '';
			this.campaign.media.file = '';
			this.campaign.media.url = '';
			this.campaign.media.type = '';
			this.$refs.mediaUpload.value = null;
		},
		onHandlePickMedia() {
			this.$refs.mediaUpload.click();
		},
		onFilePicked(evt) {
			const files = evt.target.files;

			if (!files.length) {
				return;
			}

			this.campaign.media.name = '';
			this.campaign.media.file = '';
			this.campaign.media.url = '';
			this.videoState = enumVideoState.LOADING;

			if (files[0] !== undefined) {
				this.campaign.media.name = files[0].name;
				this.campaign.media.type = this.checkMediaType(files[0].type);
				if (this.campaign.media.name.lastIndexOf('.') <= 0) {
					return;
				}
				const fr = new FileReader();
				fr.readAsDataURL(files[0]);
				fr.addEventListener('load', () => {
					this.campaign.media.url = fr.result;
					this.campaign.media.file = files[0];
					this.videoState = enumVideoState.LOADED;
				});
			} else {
				this.campaign.media.name = '';
				this.campaign.media.file = '';
				this.campaign.media.url = '';
			}
		},
		checkMediaType(type) {
			return type.split('/')[0];
		},
		formatDate(date) {
			if (!date) return null;

			const [year, month, day] = date.split('-');
			return `${month}/${day}/${year}`;
		},
		formatTime(time) {
			if (time) return time;
			const date = moment();
			const [year, month, day] = date.split('-');
			return `${month}/${day}/${year}`;
		},
		parseDate(date) {
			if (!date) return null;

			const [month, day, year] = date.split('/');
			return `${year}-${month.padStart(2, '0')}-${day.padStart(2, '0')}`;
		},
		showError(field) {
			return this.errors.has(field) ? 'has-error' : '';
		},
		async nextStep() {
			const isValid = await this.checkValidatorOnStep(`n-step${this.currentStep}`);
			if (isValid) {
				return this.checkCurrentStep(this.currentStep, true);
			}
			return false;
		},
		prevStep() {
			this.checkCurrentStep(this.currentStep, false);
		},
		canCelCampaign() {
			this.isConfirmAction = true;
			this.dialogMessage = this.localed.leave;
			this.action = enumAction.CANCEL_CREATE;
		},
		checkValidatorOnStep(scope) {
			return this.$validator.validateAll(scope).then(valid => {
				if (!valid) {
					return false;
				}
				return true;
			});
		},
		checkCurrentStep(step, action) {
			if (action) {
				if (this.currentStep < this.maxStep) {
					this.currentStep++;
				}
			} else {
				if (this.currentStep > this.minStep) {
					this.currentStep--;
				}
			}
		},
		checkValidators() {
			this.$validator.validate().then(valid => {
				if (!valid) {
					// do stuff if not valid.

				}
			});
		},
		makeDataForApi() {
			return {
				client_id: this.clientSelected._id,
				name: this.campaign.clientInfo.name,
				file: this.campaign.media.file,
				media_file: null,
				status: enumActive.ACTIVE,
				description: this.campaign.clientInfo.description,
				attributes: {
					nearby: (this.campaign.campaignInfo.nearby || []).map(item => item.id),
					location: {
						city: this.campaign.campaignInfo.city,
						districts: (this.campaign.campaignInfo.district || []).map(item => item.id),
					},
					times: {
						from: this.campaign.campaignInfo.times.from,
						to: this.campaign.campaignInfo.times.to
					},
					budget: this.campaign.campaignInfo.budget
				},
				available: {
					from: this.campaign.campaignInfo.available.from,
					to: this.campaign.campaignInfo.available.to
				},
				balance: this.campaign.campaignInfo.costInfo.totalCost
			};
		},
		async onCreateCampaing() {
			const isValid = await this.checkValidatorOnStep(`n-step${this.currentStep}`);
			if (!isValid) {
				return;
			}
			const data = this.makeDataForApi();
			this.editAd(data).then(res => {
				if (res.errors) {
					return this.$errorHandler.showServerErrors(res.errors, this);
				}
				this.$notify.success('Campaign created successfully');
				return this.$router.push({name: "Ads-Management"});
			});
		},
		getClientList() {
			return this.apiGetClientList().then(response => {
				return response.clients;
			}).then(response => {
				return response.data;
			});
		},
		async setClientList() {
			const clientListFromApi = await this.getClientList();
			this.clientList = clientListFromApi;
		},
		onChangeClient(client) {
			this.clientSelected = client;
		},
		onInitMap() {

		},
		getRandomIntInclusive(min, max) {
			min = Math.ceil(min);
			max = Math.floor(max);
			return Math.floor(Math.random() * (max - min + 1)) + min;
		},
		formatNumber(num, pattern) {
			const str = num.toString();
			return str.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		},
		onCalculateCost() {
			const costInfo = {
				devices: this.mapMarkers.length,
				viewer: this.formatNumber(this.getRandomIntInclusive(1000, 5000)),
				impressions: this.formatNumber(this.getRandomIntInclusive(10, 500)),
				totalCost: this.formatNumber(this.getRandomIntInclusive(50, 1000))
			};
			this.campaign.campaignInfo.costInfo = costInfo;
		},
		async onChangeCity(evt) {
			if (this.campaign.campaignInfo.city === evt._id) return;
			this.onCalculateCost();
			let center = evt.central.coordinates;
			this.mapCenter = [center[1], center[0]];
			this.campaign.campaignInfo.district = []; // reset
			this.getDistricts(evt._id);
		},
		onChangeDistricts(evt) {
			this.onCalculateCost();
		},
		onChangeNearbys(evt) {
			this.onCalculateCost();
		},
		onChangeBudget(evt) {
			this.onCalculateCost();
		},
		onChangeStartDate(evt) {
			this.isShowStartDate = false;
			this.startDateTime = +moment.utc(`${evt} ${this.fromTimeFormat}`, 'YYYY/MM/DD HH:mm');
			this.onCalculateCost();
		},
		onChangeEndDate(evt) {
			this.isShowEndDate = false;
			this.endDateTime = +moment.utc(`${evt} ${this.toTimeFormat}`, 'YYYY/MM/DD HH:mm');
			this.onCalculateCost();
		},
		onChangeStartTime(evt) {
			const timeFromNumberArray = evt.split(':');
			this.campaign.campaignInfo.times.from = timeFromNumberArray[0] * 60 + parseInt(timeFromNumberArray[1]);
			this.startDateTime = +moment.utc(`${this.fromDateFormat} ${this.evt}`, 'YYYY/MM/DD HH:mm');
			this.onCalculateCost();
		},
		onChangeEndTime(evt) {
			const timeToNumberArray = evt.split(':');
			this.campaign.campaignInfo.times.to = timeToNumberArray[0] * 60 + parseInt(timeToNumberArray[1]);
			this.endDateTime = +moment.utc(`${this.toDateFormat} ${evt}`, 'YYYY/MM/DD HH:mm');
			this.onCalculateCost();
		},
		onDoAction(action) {
			switch (action) {
			case enumAction.CANCEL_CREATE:
				this.$router.push({name: 'Ads-Management'});
				break;

			case enumAction.DELETE_MEDIA:
				return this.onDeleteMedia();

			default:
				break;
			}
		},
		onNoAction() {
			this.isConfirmAction = false;
			this.action = null;
			this.dialogMessage = null;
		},
		getCities() {
			this.loadCities().then(res => {
				if (res.errors) {
					console.log(res.errors);
					return;
				}
				this.cities = res.cities;
			});
		},
		getDevices() {
			this.loadDevices().then(res => {
				if (res.errors) {
					console.log(res.errors);
					return;
				}
				this.mapMarkers = res.devices.map(item => {
					return {
						position: [item.location.coordinates[1], item.location.coordinates[0]],
					};
				});
			});
		},
		async getDistricts(city) {
			return this.loadDistricts({city}).then(res => {
				if (res.errors) {
					console.log(res.errors);
					return;
				}
				this.districts = (res.districts || []).map(item => {
					return {id: item._id, text: item.name, center: item.central};
				});
			});
		},
	}
};

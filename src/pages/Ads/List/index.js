import {axios} from "@/common/helpers/api";
import {BASE_API_URL} from "@/common/configs/api";
import moment from 'moment';
import {setLocale} from "assets/locale/utils";
import {mapActions} from "vuex";
import {ACTIONS as AD_ACTIONS} from "@/store/modules/ad/const";
import './styles.behavior.scss';

const enumType = {
	DATE: 'DATE',
	TIME: 'TIME'
};

const enumStatus = {
	ACTIVE: 'Active',
	INACTIVE: 'Inactive'
};

const enumAction = {
	DELETE_SELECTED: 'DELETE_SELECTED',
	REFRESH: 'REFRESH',
	DELETE: 'DELETE',
	CHANGE_STATUS: 'CHANGE_STATUS'
};

export default {
	name: 'List',
	data() {
		return {
			nSwitchColor: {
				active: '#46b28b',
				inactive: '#949494'
			},
			isConfirmAction: false,
			dialogMessage: '',
			itemDoAction: '',
			itemDoActionStatus: null,
			action: '',
			totalAd: 0,
			pagination: {
				sortBy: 'name'
			},
			search: '',
			adList: [],
			selected: [],
			headers: [
				{text: 'Campaign Name', value: 'name', class: 'text-xs-left'},
				{text: 'Client', value: 'client', class: 'text-xs-left'},
				{text: 'Type', value: 'type', class: 'text-xs-left'},
				{text: 'Created date', value: 'createdDate', class: 'text-xs-left'},
				{text: 'Status', value: 'status', class: 'text-xs-left'},
				{text: 'Actions', value: 'actions'}
			],
		};
	},
	watch: {
		params: {
			handler() {
				this.getDataFromApi().then(data => {
					this.adList = data.items;
					this.totalAd = data.total;
				});
			},
			deep: true
		}
	},
	mounted() {
		this.getDataFromApi().then(data => {
			this.adList = data.items;
			this.totalAd = data.total;
		});
	},
	computed: {
		params(nv) {
			return {
				...this.pagination,
				query: this.search
			};
		}
	},
	methods: {
		...mapActions({
			removeAd: AD_ACTIONS.REMOVE,
			apiRemoveAdItems: AD_ACTIONS.REMOVE_ITEMS,
			apiUpdateStatusAd: AD_ACTIONS.UPDATE_STATUS,
		}),
		thClasses() {
			let sortable = 'sortable';
			if (this.headers.value === 'actions') sortable = '';
			return [`column ${sortable} text-xs-left`, this.pagination.descending ? 'desc' : 'asc', this.header.value === this.pagination.sortBy ? 'active' : ''];
		},
		onDoAction(action) {
			console.log(action);
			switch (action) {
			case enumAction.DELETE:
				return this.onDeleteItem();

			case enumAction.DELETE_SELECTED:
				return this.onDeleteItems();

			case enumAction.REFRESH:
				return this.onRefresh();

			case enumAction.CHANGE_STATUS:
				return this.onChangeStatus();

			default:
				break;
			}
		},

		onNoAction() {
			this.isConfirmAction = false;
			switch (this.action) {
			case enumAction.CHANGE_STATUS:
				return this.setAndClearItemDoAction();

			default:
				break;
			}
		},

		setAndClearItemDoAction() {
			this.itemDoAction.isActive = !this.itemDoAction.isActive;
			this.itemDoAction = null;
			this.itemDoActionStatus = null;
		},

		onEditItem(id) {
			this.$router.push({name: 'Ads-Edit', params: {id: id}});
		},

		onAddNew() {
			this.$router.push({name: 'Ads-Add'});
		},

		toCapitalizeFirstChar(str) {
			return str.charAt(0).toUpperCase() + str.slice(1);
		},

		onSwitch(item, isActive) {
			let currentItem = this.adList.find(a => a.id === item.id);

			this.itemDoActionStatus = isActive === true
				? enumStatus.ACTIVE.charAt(0).toUpperCase() + enumStatus.ACTIVE.slice(1)
				: enumStatus.INACTIVE.charAt(0).toUpperCase() + enumStatus.INACTIVE.slice(1);
			this.isConfirmAction = true;
			const actionString = this.itemDoActionStatus.toLowerCase() === 'active' ? 'activate' : 'deactivate';
			this.dialogMessage = `${setLocale.change_status(actionString, currentItem.name)}`;
			this.action = enumAction.CHANGE_STATUS;
			this.itemDoAction = currentItem;

		},

		onChangeStatus() {
			const newStatus = this.itemDoActionStatus.toLowerCase();
			this.apiUpdateStatusAd({id: this.itemDoAction.id, status: newStatus}).then(response => {
				if (response.errors) {
					return this.$errorHandler.showServerErrors(response.errors, this);
				}
				this.isConfirmAction = false;
				this.changeStatus(this.itemDoAction, newStatus);
				this.$notify.success(`${setLocale.common.notify.success('ad', 'update status')}`);
			});
		},

		changeStatus(ad, newStatus) {
			ad.isActive = !ad.isActive;
			this.adList = this.updateFromArray(this.adList, ad, newStatus);
		},

		updateFromArray(original, ad, status) {
			const newAdList = original;
			newAdList.map(adItem => {
				if (adItem.id === ad.id) {
					adItem.status = status;
					adItem.isActive = !adItem.isActive;
				}
			});
			return newAdList;
		},

		onConfirmDeleteItems(items) {
			if(this.selected.length) {
				this.dialogMessage = 'Do you want to delete all campaigns have been selected';
				this.isConfirmAction = true;
				this.action = enumAction.DELETE_SELECTED;
			}
		},

		onConfirmRefresh() {
			this.dialogMessage = `${setLocale.load_data}`;
			this.isConfirmAction = true;
			this.action = enumAction.REFRESH;
		},

		onConfirmDeleteItem(item) {
			this.isConfirmAction = true;
			this.dialogMessage = `${setLocale.common.messages.delete(item.name)}`;
			this.itemDoAction = item;
			this.action = enumAction.DELETE;
		},

		onRefresh() {
			this.getDataFromApi().then(data => {
				this.adList = data.items;
				this.totalAd = data.total;
				this.isConfirmAction = false;
				this.$notify.success(`${setLocale.common.notify.success('latest data', 'get')}`);
			});
		},

		onDeleteItem() {
			if (!this.itemDoAction) {
				return;
			}
			this.removeAd(this.itemDoAction.id).then(res => {
				if (res.errors) {
					return this.$errorHandler.showServerErrors(res.errors, this);
				}
				this.isConfirmAction = false;
				this.removeAdItem(this.itemDoAction.id);
				this.$notify.success(`${setLocale.common.notify.success('ad', 'remove')}`);
			});
		},

		onDeleteItems() {
			this.apiRemoveAdItems(this.selected).then(response => {
				if (response.errors) {
					return this.$errorHandler.showServerErrors(response.errors, this);
				}
				this.isConfirmAction = false;
				this.removeAdItems(this.selected);
				this.$notify.success(`${setLocale.common.notify.success('ad', 'remove')}`);
			});
		},

		removeAdItem(id) {
			const itemDeleteIndex = this.adList.findIndex((item) => (item.id === id));
			console.log(itemDeleteIndex);
			if (itemDeleteIndex !== -1) {
				this.adList.splice(itemDeleteIndex, 1);
				this.itemDoAction = null;
			}
		},

		removeAdItems(removeAdList) {
			this.adList = this.removeFromArray(this.adList, removeAdList);
		},

		removeFromArray(original, remove) {
			const removeAdList = original;
			this.selected = [];
			return removeAdList.filter(ad => {
				return !remove.includes(ad);
			});
		},

		getDataFromApi() {
			this.loading = true;
			return new Promise(async (resolve, reject) => {
				const {
					sortBy,
					descending,
				} = this.pagination;
				let search = this.search.trim().toLowerCase();

				const dataFromApi = await this.getAdList();
				let items = this.makeDataForUI(dataFromApi.data);

				if (search) {
					items = items.filter(item => {
						return Object.values(item)
							.join(",")
							.toLowerCase()
							.includes(search);
					});
				}

				if (this.pagination.sortBy) {
					items = items.sort((a, b) => {
						const sortA = a[sortBy];
						const sortB = b[sortBy];

						if (descending) {
							if (sortA < sortB) return 1;
							if (sortA > sortB) return -1;
							return 0;
						} else {
							if (sortA < sortB) return -1;
							if (sortA > sortB) return 1;
							return 0;
						}
					});
				}

				const total = items.length;

				setTimeout(() => {
					this.loading = false;
					resolve({
						items,
						total
					});
				}, 300);
			});
		},

		formatMoment(type, timeStamp) {
			const currentType = type;
			const dateTime = moment(timeStamp);

			switch (currentType) {
			case enumType.DATE:
				return dateTime.format('MMM Do YYYY');

			case enumType.TIME:
				return dateTime.format('hh:mm A');

			default:
				break;
			}

		},

		makeDataForUI(data) {
			return data.map((item) => ({
				id: item._id,
				client: item.client_id.name,
				name: item.name,
				type: item.file.file_type,
				pointsUsage: item.client_id.balance,
				createdDate: {
					time: this.formatMoment(enumType.TIME, item.created_time),
					date: this.formatMoment(enumType.DATE, item.created_time)
				},
				status: item.status.charAt(0).toUpperCase() + item.status.slice(1),
				isActive: item.status === 'active'
			}));
		},

		getAdList() {
			return axios.get(`${BASE_API_URL}/ads`).then(response => {
				return response.data;
			}).then(response => {
				return response;
			});
		},

		toggleAll() {
			if (this.selected.length) this.selected = [];
			else this.selected = this.adList.slice();
		},

		changeSort(column) {
			if (column === 'actions') return;
			if (this.pagination.sortBy === column) {
				this.pagination.descending = !this.pagination.descending;
			} else {
				this.pagination.sortBy = column;
				this.pagination.descending = false;
			}
		}
	}
};

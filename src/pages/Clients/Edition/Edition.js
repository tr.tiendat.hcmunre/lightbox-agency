"use strict";
import {mapActions} from 'vuex';
import {setLocale} from "assets/locale/utils";
import {ACTIONS} from "@/store/modules/client/const";

const enumActive = {
	INACTIVE: 'inactive',
	ACTIVE: 'active'
};

export default {
	data: () => ({
		loading: false,
		options: [
			{id: 'active', text: 'Active'},
			{id: 'inactive', text: 'Inactive'}
		],
		formData: {
			name: null,
			status: enumActive.ACTIVE,
			email: null,
			phone: null,
			address: null
		},
		isSubmitting: false,
	}),
	computed: {
	},
	components: {},
	watch: {
		$route(to, from) {
			if (to.path !== from.path) {
				this.reload();
			}
		},
		loading(val) {
			this.loaded = !val;
		},
	},
	updated() {
		let that = this;
		$(this.$el).find('.select2')
			.select2({data: this.options, minimumResultsForSearch: Infinity})
			.val(this.formData.status)
			.one('change', function () {
				that.formData.status = this.value;
			});
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$route.params.id ? this.$const.CLIENTS_EDITION : this.$const.CLIENTS_CREATION);
	},
	created() {
		this.reload();
		const dict = {
			custom: setLocale.Client.validation
		};

		this.$validator.localize('en', dict);
	},
	mounted() {
	},

	methods: {
		...mapActions({
			loadClient: ACTIONS.LOAD,
			editClient: ACTIONS.EDIT_CLIENT
		}),
		reset() {
			this.formData = {
				name: null,
				status: enumActive.ACTIVE,
				email: null,
				phone: null,
				address: null
			};
		},
		reload() {
			this.reset();
			if (this.$route.params.id) {
				this.loading = true;
				this.loadClient(this.$route.params.id).then(res => {
					this.formData = res.client;
					this.loading = false;
				});
			}
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		showError(field) {
			return this.errors.has(field) ? 'has-error' : '';
		},
		validateForm() {

			this.$validator.validateAll().then((result) => {
				if (result) {
					if (!this.isSubmitting) {
						this.isSubmitting = true;
						this.editClient(this.formData).then(res => {
							if (res.errors) {
								return this.$errorHandler.showServerErrors(res.errors, this);
							}

							this.$notify.success(`${setLocale.common.notify.success('client', 'save')}`);
							if (this.$route.query.returnUrl) {
								return this.$router.push({name: this.$route.query.returnUrl});
							}
							return this.$router.push({name: "Clients-Management"});
						});
					}
				}
			}
			);
		},
	}
}
;

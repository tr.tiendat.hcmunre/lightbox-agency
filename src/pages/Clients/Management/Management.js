"use strict";
import {mapGetters, mapActions} from 'vuex';
import User from '../../Profile/User/User.vue';
import {GETTERS, ACTIONS} from '../../../store/const';
import {BASE_API_URL} from '@/common/configs/api';
import {ACTIONS as AD_ACTIONS} from '../../../store/modules/ad/const';
import {ACTIONS as CLIENT_ACTIONS} from '../../../store/modules/client/const';
import {setLocale} from "assets/locale/utils";

export default {
	data: () => ({
		loaded: true,
		showConfirm: false,
		clientDelete: null,
		deleteMsg: ''
	}),
	components: {
		'user-info': User,
	},
	computed: {
		...mapGetters({
			loading: GETTERS.LOADING
		})
	},
	watch: {
		loading(val) {
			this.loaded = !val;
		},
	},

	methods: {
		...mapActions({
			removeClient: CLIENT_ACTIONS.REMOVE,
			loadAds: AD_ACTIONS.LOAD_ALL
		}),
		...mapActions({
			load: ACTIONS.LOAD
		}),
		reload() {
			this.load();
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		edit(id) {
			this.$router.push({name: 'Clients-Edit', params: {id: id}});
		},
		async confirmRemove(client) {
			const ads = await this.getAds();
			const clientInAds = ads.find(ad => (ad.client_id && ad.client_id._id === client._id));
			if (clientInAds && clientInAds.client_id && clientInAds.client_id._id === client._id) {
				this.deleteMsg = `${setLocale.common.messages.cannot_delete(client.name)}`;
			} else {
				this.deleteMsg = `${setLocale.common.messages.delete(client.name)}`;
				this.clientDelete = client;
			}
			$("#modal-confirm").modal('show');
		},
		async getAds() {
			return this.loadAds().then(res => {
				return (res.ads && res.ads.data) || [];
			}).then(ads => {
				return ads;
			}).catch(err => {
				console.log('Errors load ads: ', err);
				return false;
			});
		},
		remove() {
			if (!this.clientDelete) {
				return;
			}

			this.removeClient(this.clientDelete._id).then(res => {
				// this.$error
				if (res.errors) {
					return this.$errorHandler.showServerErrors(res.errors, this);
				}
				this.table.ajax.reload(null, false); // user paging is not reset on reload
				this.clientDelete = null;
				$("#modal-confirm").modal('hide');
				this.$notify.success(`${setLocale.common.notify.success('client', 'remove')}`);
			});
		},
		cancel() {
			this.showConfirm = false;
			this.clientDelete = null;
		}
	},
	mounted() {
		var that = this;
		this.reload();
		var table = this.table = $(this.$el).find('#clients').DataTable({
			"serverSide": true,
			"processing": true,
			"pageLength": 10,
			"ajax": $.fn.dataTable.pipeline({
				url: `${BASE_API_URL}/clients`
			}),
			'info': true,
			'autoWidth': false,
			"columns": [
				{"data": "name", "width": "20%"},
				{"data": "email"},
				{"data": "phone"},
				{"data": "address", "width": "20%"},
				{"data": "balance", "width": "80px"},
				{
					"data": "status",
					"render": function (data, type, row) {
						return data.charAt(0).toUpperCase() + data.slice(1);
					}
				},
				{
					"data": null,
					"width": "70px",
					"defaultContent": '<a class="btn btn-info edit"><i class="fa fa-pencil"></i></a> ' +
						'<a  class="btn btn-danger delete"><i class="fa fa-trash"></i></a>'
				}
			],
			"language": {
				"search": "",
				"searchPlaceholder": "Filter",
			},
			"dom": '<"pull-left"f><"col-md-6"iB><"fix-table"t><"clear"><"col-xs-6 col-md-6"l><"col-xs-6 col-md-6"p>'
		});

		$('#clients tbody').on('click', 'a', function () {
			var data = table.row($(this).parents('tr')).data();
			if ($(this).hasClass('edit')) {
				that.edit(data._id);
			} else if ($(this).hasClass('delete')) {
				that.confirmRemove(data);
			}
		});
		this.table = table;
	},
	updated() {
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$const.CLIENTS_MANAGEMENT);
	},
};

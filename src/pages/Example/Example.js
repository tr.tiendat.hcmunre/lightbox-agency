import './demo.scss';
import SideBar from './components/sidebar';
import NavBar from "./components/navbar";
import Map from "components/Map/Map.vue";

export default {
	components: {
		'side-bar': SideBar,
		'nav-bar': NavBar,
		'm-map': Map
	},
	data() {
		return {
			isMenuOpen: false,
			isOpenDrawer: true,
			isProgressOpen: true,
			switchModel: {
				demo1: {
					values: [
						'demo1',
						'demo2',
					],
					selected: []
				},
				demo2: {
					value1: true,
					value2: false
				}
			},
			checkedModel: {
				demo1: {
					values: [
						'demo1',
						'demo2',
					],
					selected: []
				},
				demo2: {
					value1: true,
					value2: false
				}

				// demo1: true,
				// demo2: false
			},
			radioModel: {
				selected: 'demo1',
				values: [
					'demo1',
					'demo2'
				]
			},
			sliderModel: 20,
			isDialogOpen: false,
			isConfirmDialogOpen: false,
			autoOptions: [
				{ id: 1, text: 'Option 1' },
				{ id: 2, text: 'Option 2' }
			],
			autoSelected: 1,
			mapMarkers: [
				// {
				// 	position: [61.0666922, -107.9917071],
				// 	text: 12
				// },
				{
					position: [43.7170226, -79.4197830350134],
					text: 12
				},
				{
					position: [43.7270226, -79.4197830350134],
					text: 12
				},
				// {
				// 	position: [61.0346139, -107.9917071],
				// 	text: 13
				// }
			]

		};
	},
	mounted() {
		this.initLayout();
	},
	methods: {
		initLayout() {
			// Tabs
		},
		onCheckBoxChange(evt) {
			console.log("CheckBox demo 1 Selected", this.checkedModel.demo1.selected);

			console.log("CheckBox demo 2 Selected", this.checkedModel.demo2);
		},
		onSwitchChange(evt) {
			console.log("Switch demo 1 Selected", this.switchModel.demo1.selected);

			console.log("Switch demo 2 Selected", this.switchModel.demo2);
		},
		onDialogClosed (value) {
			console.log(value);
		},
		onAutoCompleteChange(evt) {
			console.log("autocomplete changed", evt, this.autoSelected);
		}
	}
};

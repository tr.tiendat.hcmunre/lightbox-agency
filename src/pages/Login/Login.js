"use strict";
import './Login.scss';
import { mapActions, mapGetters } from 'vuex';
import { GETTERS, ACTIONS } from '../../store';
import {setLocale} from "assets/locale/utils";

export default {
	name: 'Login',
	data: () => ({
		form: {
			email: '',
			password: '',
			remember_me: false
		},
		isSaving: false,
	}),
	computed: {
		...mapGetters({
			isAuthenticated: GETTERS.IS_AUTHENTICATED
		}),
		isError () {
			return this.hasErrors('email') || this.hasErrors('password');
		}
	},
	created() {
		const dict = {
			custom: setLocale.LogIn.validation
		};

		this.$validator.localize('en', dict);
	},
	updated() {
		if (this.isAuthenticated) {
			this.$router.push('Dashboard');
		}
		this.updateUI();
	},
	mounted() {
		if (this.isAuthenticated) {
			return this.$router.push('Dashboard');
		}

		if(this.$route.query.verify) {
			this.$notify.success(`${setLocale.LogIn.notify.success.emailVerify((this.$route.query.verify))}`, {
				position: 'bottom-center'
			});
		}
	},
	methods: {
		...mapActions({
			signin: ACTIONS.SIGNIN
		}),
		updateUI() {
		},
		submit(event) {
			event.preventDefault();
			if(this.isSaving) {
				return;
			}
			this.$validator.validateAll().then(isValid => {
				if (isValid) {
					this.isSaving = true;
					this.signin(this.form).then(res => {
						this.isSaving = false;
						if (res.errors) {
							return this.$errorHandler.showServerErrors(res.errors, this);
						}
						return this.$router.push({name: 'Profile'});
					});
				}
			});
		}
	}
};

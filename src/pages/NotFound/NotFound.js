"use strict";
import { mapGetters, mapActions } from 'vuex';
import { GETTERS, ACTIONS } from '../../store/const';

export default {
	data: () => ({
		loaded: true
	}),
	components: {},
	computed: {
		...mapGetters({
			loading: GETTERS.LOADING
		})
	},
	watch: {
		loading(val) {
			this.loaded = !val;
		}
	},

	mounted() {
		this.reload();
	},

	methods: {
		...mapActions({
			load: ACTIONS.LOAD
		}),
		reload() {
			this.load();
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		}
	},
	updated() {
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$const.CLIENTS);
	}
};

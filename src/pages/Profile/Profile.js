"use strict";
import { mapGetters, mapActions } from 'vuex';
import SettingProfile from './SettingProfile/SettingProfile.vue';
import User from './User/User.vue';
import { GETTERS, ACTIONS } from '../../store/const';

export default {
	data: () => ({
		loaded: true
	}),
	components: {
		'user-info': User,
		'setting-profile': SettingProfile
	},
	computed: {
		...mapGetters({
			loading: GETTERS.LOADING
		})
	},
	watch: {
		loading(val) {
			this.loaded = !val;
		}
	},

	mounted() {
		this.reload();
	},

	methods: {
		...mapActions({
			load: ACTIONS.LOAD
		}),
		reload() {
			this.load();
		}
	},
	updated() {
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$const.PROFILE);
	}
};

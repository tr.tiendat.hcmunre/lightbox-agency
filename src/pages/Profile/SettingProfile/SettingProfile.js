"use strict";
import { mapActions, mapGetters } from 'vuex';
import { GETTERS, ACTIONS } from '@/store';
import {setLocale} from "assets/locale/utils";

export default {
	data: () => ({
		phoneFormat: '+1-###-###-####', // format phone number of Canada
		formData: {
			name: null,
			logo: 'null',
			email: 'agency1@test.com',
			phone: null,
			address: null
		},
		uploadError: null,
		uploadFieldName: 'logo',
		avatar: null,
		isSubmitting: false
	}),
	computed: {
		...mapGetters({
			currentUser: GETTERS.CURRENT_USER
		}),
	},
	created() {
		this.setFormData();
		const that = this;
		$(document).on("click", "i.delete-img", function (evt) {
			if (that.showError('file') === '') evt.preventDefault();
			$('.preview-img').replaceWith('<div class="preview-img"></div>');
			$('.btn-upload input[type=file]').val('');
			that.clearErrorField('file');
		});
		$(document).on("change", ".btn-success", function (evt) {
			if (that.showError('file') === '') evt.preventDefault();
			var uploadFile = $(this);
			var files = this.files ? this.files : [];
			if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

			if (/^image/.test(files[0].type)) { // only image file
				var reader = new FileReader(); // instance of the FileReader
				reader.readAsDataURL(files[0]); // read the local file

				reader.onloadend = function () {
					uploadFile.closest(".form-group").find('.preview-img').css("background-image", "url(" + this.result + ")").addClass('has-img');
				};
			}
		});
	},
	methods: {
		...mapActions({
			updateProfile: ACTIONS.UPDATE_PROFILE,
		}),
		setFormData() {
			this.formData.email = this.currentUser.email;
			this.formData.name = this.currentUser.name;
			this.formData.phone = this.currentUser.phone;
			this.formData.address = this.currentUser.address;
		},
		resetForm(form) {
			this.errors.clear();
			this.formData.logo = null;
			$('.preview-img').replaceWith('<div class="preview-img"></div>');
		},
		saveFormData() {

		},
		validateForm(event) {
			this.$validator.validateAll().then((result) => {
				if (result) {
					const {
						formData
					} = this;
					this.submitForm(formData, event);
				}
			});
		},
		showError(field) {
			return this.errors.has(field) ? 'has-error' : '';
		},
		clearErrorField(field) {
			this.errors.remove(field);
		},
		submitForm(formData, event) {
			this.formData.logo = this.$refs.file.files[0];
			if (!this.isSubmitting) {
				this.isSubmitting = true;
				this.updateProfile(formData).then(res => {
					if (res.errors) {
						return this.$errorHandler.showServerErrors(res.errors, this);
					}

					this.$notify.success(setLocale.Profile.notify.success.updated);
					this.isSubmitting = false;
					return this.resetForm(event.target);
				});
			}
		},
	},
	mounted() {
		this.resetForm();
		$(this.$el).find('[data-mask]').inputmask();
	},
};

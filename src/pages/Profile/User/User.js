"use strict";

import { mapGetters } from "vuex";
import { GETTERS } from "../../../store";

export default {
	data: () => ({
		user: 'LB Agency',
		desJob: 'Software Engineer',
		remainningPoints: 0,
		usagePoints: 0
	}),
	computed: {
		...mapGetters({
			avatar: GETTERS.AVATAR,
			currentUser: GETTERS.CURRENT_USER
		})
	},
	components: {

	},
	mounted() {
		this.setDataUser();
	},
	watch: {

	},
	methods: {
		getAvatar() {
			return this.avatar;
		},
		setDataUser() {
			this.remainningPoints = this.currentUser.balance;
			this.user = this.currentUser.name;
		}
	},
};

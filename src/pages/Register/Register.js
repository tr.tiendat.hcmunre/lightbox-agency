import { mapGetters, mapActions } from "vuex";
import {ACTIONS, GETTERS, MUTATES} from "../../store/const";
import {setLocale} from "assets/locale/utils";
import './Register.scss';
import UserInfo from "./Steps/UserInfo.vue";
import CompanyInfo from "./Steps/CompanyInfo.vue";
import PaymentInfo from "./Steps/PaymentInfo.vue";

export default {
	name: 'Register',
	components: {
		'user-info': UserInfo,
		CompanyInfo,
		'payment-info': PaymentInfo
	},
	data: () => ({
		isValid: false,
		form: {
			first_name: '',
			last_name: '',
			email: '',
			password: '',
			confirmed_password: '',
			agree_term: false,
			company_info: {
				name: '',
				address: '',
				country: '',
				province: '',
				city: '',
				time_zone: '',
			},
			contact_info: {
				first_name: '',
				last_name: '',
				phone: '',
				email: ''
			},
			payment_info: {
				currency: '',
				card_number: '',
				expired: '',
				secret_code: '',
				post_code: '',
				country: '',
			}
		},
		currentStep: 1
	}),
	computed: {
		...mapGetters({
			loading: GETTERS.LOADING,
			saving: GETTERS.SAVING,
			isAuthenticated: GETTERS.IS_AUTHENTICATED
		}),
		canSave() {
			return !this.isValid || this.saving;
		}
	},
	watch: {
		'form.agree_term'(value) {
			this.isValid = value;
		}
	},
	updated() {
	},
	created() {
		const dict = {
			custom: setLocale.Register.validation
		};

		this.$validator.localize('en', dict);
	},
	mounted() {
		if (this.isAuthenticated) {
			this.$router.push('Dashboard');
		}
	},
	methods: {
		...mapActions({
			register: ACTIONS.REGISTER,
			signin: ACTIONS.SIGNIN
		}),
		next(nextStep) {
			this.currentStep = nextStep;
		},
		previous(step) {
			this.currentStep = step;
		},
		submit() {
			this.$store.commit(MUTATES.SAVING);
			this.register(this.form).then(res => {
				if (res.errors) {
					this.$store.commit(MUTATES.SAVED);
					return this.$errorHandler.showServerErrors(res.errors, this);
				}

				this.$notify.success(`${setLocale.Register.notify.success.account}`, {
					icon: 'flag-o'
				})

				this.signin({
					email: this.form.email,
					password: this.form.password,
				}).then(res => {
					this.$store.commit(MUTATES.SAVED);
					if (res.errors) {
						return this.$errorHandler.showServerErrors(res.errors, this);
					}
					return this.$router.push({name: 'Ads-Add'});
				});
			});
		}
	}
};

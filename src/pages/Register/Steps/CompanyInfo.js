import {mapActions} from "vuex";
import {ACTIONS} from "../../../store";

export default {
	name: 'company-info',
	props: {
		companyInfo: {
			type: Object,
		},
		contactInfo: {
			type: Object,
		},
	},
	watch: {
		'companyInfo.country'(value, oldValue) {
			if(value === oldValue || !value) {
				return;
			}
			this.companyInfo.province = '';
			this.companyInfo.city = '';
			this.cities = [];// reset city list
			this.states = [];// reset state list
			this.getStates(value);
		},
		'companyInfo.province'(value, oldValue) {
			if(value === oldValue || !value) {
				return;
			}

			this.companyInfo.city = '';
			this.cities = [];// reset city list
			this.getCities(value);
		}
	},
	data: () => ({
		timeZones: [],
		countries: [],
		states: [],
		cities: [],

	}),
	async created() {
		await this.loadMaster();
	},
	methods: {
		...mapActions({
			loadTimeZones: ACTIONS.GET_TIME_ZONES,
			loadCountries: ACTIONS.GET_COUNTRIES,
			loadStates: ACTIONS.GET_STATES,
			loadCities: ACTIONS.GET_CITIES
		}),
		next() {
			event.preventDefault();
			this.$validator.validateAll().then(isValid => {
				if (isValid) {
					this.$emit('next');
				}
			});
		},
		previous() {
			this.$emit('prev');
		},
		async loadMaster() {
			return Promise.all([
				this.loadTimeZones({all: true}),
				this.loadCountries({all: true})
			]).then(res => {
				this.timeZones = res[0].data;
				this.countries = res[1].data;
			});
		},
		getStates(country = null) {
			this.loadStates({
				all: true,
				country
			}).then(res => {
				this.states = res.data || [];
			});
		},
		getCities(state = null) {
			this.loadCities({
				all: true,
				state
			}).then(res => {
				this.cities = res.data || [];
			});
		}
	}
};

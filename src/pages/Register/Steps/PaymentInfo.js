export default {
	name: 'payment-info',
	props: {
		paymentInfo: {
			type: Object,
		},
	},
	data: () => ({
		currencyOptions: [
			{ id: 'vnd', text: 'VND' },
			{ id: 'usd', text: 'USD' }
		],
		countries: [
			{_id: 'vn', text: 'VietNam'},
			{_id: 'ca', text: 'Canada'}
		],
	}),
	methods: {
		next(isValidate = true) {
			event.preventDefault();

			if(!isValidate) {
				return this.$emit('next');
			}

			return this.$validator.validateAll('payment').then(isValid => {
				if (isValid) {
					this.$emit('next');
				}
			});
		},
		previous() {
			this.$emit('prev');
		},
		hasErrors(prop) {
			return this.errors.has(`payment.${prop}`);
		},
		getError(prop) {
			return this.errors.first(`payment.${prop}`);
			// return this.errors.has(prop);
		}
	}
};

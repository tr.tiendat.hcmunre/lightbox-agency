import {mapActions} from "vuex";
import {ACTIONS as USER_ACTIONS} from '../../../store/modules/users/const';

export default {
	name: 'user-info',
	props: {
		userInfo: {
			type: Object,
		}
	},
	methods: {
		...mapActions({
			checkUser: USER_ACTIONS.CHECK_USER_EXISTS
		}),
		next() {
			event.preventDefault();
			this.$validator.validateAll().then(isValid => {
				if (isValid) {
					this.checkUser(this.userInfo.email).then(res => {
						if(res.exists) {
							this.errors.add({
								field: 'email',
								msg: this.$t('email_exists')
							});

							return;
						}

						this.$emit('next');
					});

				}
			});
		},
		previous() {
			this.$emit('prev');
		}
	}
};

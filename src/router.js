import Vue from 'vue';
import Router from 'vue-router';
import About from '@/pages/About';
import Terms from '@/pages/Terms/Terms.vue';
import Profile from '@/pages/Profile/Profile.vue';
import Home from '@/pages/Home/Home.vue';
import Main from "@/layout/Main.vue";
import Register from "@/pages/Register/Register.vue";
import Login from "@/pages/Login/Login.vue";
import { default as ClientEdition } from '@/pages/Clients/Edition/Edition.vue';
import { default as ClientManagement } from '@/pages/Clients/Management/Management.vue';
import NotFound from '@/pages/NotFound/NotFound.vue';

import Ad from '@/pages/Ads/index.vue';
import AdItem from '@/pages/Ads/Item/index.vue';
import AdEdition from '@/pages/Ads/Edition/Edition.vue';

import Example from '@/pages/Example/Example.vue';

Vue.use(Router);

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Main,
			children: [
				{
					path: '/dashboard',
					name: 'Dashboard',
					component: Home
				},
				{
					path: '/profile',
					name: 'Profile',
					component: Profile
				},
				{
					path: '/clients/add',
					name: 'Clients-Add',
					component: ClientEdition
				},
				{
					path: '/clients/:id',
					name: 'Clients-Edit',
					component: ClientEdition
				},
				{
					path: '/clients',
					name: 'Clients-Management',
					component: ClientManagement
				},
				{
					path: "/ads",
					name: "Ads-Management",
					component: Ad
				},
				{
					path: '/ads/add',
					name: 'Ads-Add',
					component: AdItem
				},
				{
					path: '/ads/:id',
					name: 'Ads-Edit',
					component: AdEdition
				}
			]
		},
		{
			path: '/about',
			name: 'About',
			component: About
		},
		{
			path: '/terms',
			name: 'Terms',
			component: Terms
		},
		{
			path: "/register",
			name: "Register",
			component: Register
		},
		{
			path: "/login",
			name: "Login",
			component: Login
		},
		{
			path: "/not-found",
			name: "NotFound",
			component: NotFound
		},
		{
			path: "/demo",
			name: "Demo",
			component: Example
		}
	],
	scrollBehavior: (to, from, savedPosition) => (savedPosition || { x: 0, y: 0 }),
});

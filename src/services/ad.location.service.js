import { axios as api } from '../common/helpers/api';
import { API_URL, REQUEST_ACTION } from '../common/configs/api';

export function loadLocation(id) {
	return api.get(`${API_URL.LOCATION}/${id}`);
};

export function addLocation(location) {
	return api.post(`${API_URL.LOCATION}/${REQUEST_ACTION.ADD}`, location, {useFormData: true});
};

export function updateLocation(id, location) {
	return api.put(`${API_URL.LOCATION}/${id}`, location, {useFormData: true});
};

export function removeLocation(id) {
	return api.delete(`${API_URL.LOCATION}/${id}`);
};

export function loadLocations() {
	return api.get(`${API_URL.LOCATION}`);
};

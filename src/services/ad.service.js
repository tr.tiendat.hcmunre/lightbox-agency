import { axios as api } from '../common/helpers/api';
import { API_URL, REQUEST_ACTION } from '../common/configs/api';

export function loadAd(id) {
	return api.get(`${API_URL.AD}/${id}`);
};

export function addAd(ad) {
	return api.post(`${API_URL.AD}/${REQUEST_ACTION.ADD}`, ad, {useFormData: true});
};

export function updateAd(id, ad) {
	return api.put(`${API_URL.AD}/${id}`, ad, {useFormData: true});
};

export function removeAd(id) {
	return api.delete(`${API_URL.AD}/${id}`);
};

export function loadAllAd() {
	return api.get(`${API_URL.AD}`);
};

export function removeAdItems(idAdList) {
	const idList = idAdList.map(ad => {
		return ad.id;
	});
	return api.delete(`${API_URL.AD}/${REQUEST_ACTION.REMOVE}`, {params: {ids: idList}});
}

export function updateStatus(idAd, status) {
	return api.put(`${API_URL.AD}/${idAd}/status/${status}`);
}

export function getDeviceAvalables() {
	return api.get(`${API_URL.DEVICE}`);
}

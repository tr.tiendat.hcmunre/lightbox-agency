import { axios as api } from '../common/helpers/api';
import { API_URL, REQUEST_ACTION } from '../common/configs/api';

export function loadTime(id) {
	return api.get(`${API_URL.TIME}/${id}`);
};

export function addTime(time) {
	return api.post(`${API_URL.TIME}/${REQUEST_ACTION.ADD}`, time, {useFormData: true});
};

export function updateTime(id, time) {
	return api.put(`${API_URL.TIME}/${id}`, time, {useFormData: true});
};

export function removeTime(id) {
	return api.delete(`${API_URL.TIME}/${id}`);
};

export function loadTimes() {
	return api.get(`${API_URL.TIME}`);
};

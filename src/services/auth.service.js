import { axios as api } from '../common/helpers/api';
import { API_URL } from '../common/configs/api';

export const login = (email, password) => api.post(API_URL.LOGIN, null, { email, password });

export function signup(user = {}) {
	return api.post(API_URL.REGISTER, user);
}

export function signin(login = {}) {
	return api.post(API_URL.SIGNIN, login);
}

export function signout() {
	return api.get(API_URL.SIGNOUT);
}

import { axios as api } from '../common/helpers/api';
import { API_URL, REQUEST_ACTION } from '../common/configs/api';

export function loadClient(id) {
	return api.get(`${API_URL.CLIENT}/${id}`);
};

export function loadClients() {
	return api.get(`${API_URL.CLIENT}`);
}

export function addClient(client) {
	return api.post(`${API_URL.CLIENT}/${REQUEST_ACTION.ADD}`, client);
};

export function updateClient(id, client) {
	return api.put(`${API_URL.CLIENT}/${id}`, client);
};

export function removeClient(id) {
	return api.delete(`${API_URL.CLIENT}/${id}`);
};

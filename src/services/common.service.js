import { axios as api } from '../common/helpers/api';
import { API_URL } from '../common/configs/api';

export function checkExists(email = '') {
	email = encodeURIComponent(email);
	return api.get(`${API_URL.USER_EXISTS}/${email}`);
};

export function loadTimeZones(params) {
	return api.get(`${API_URL.TIME_ZONES}`, { params });
};

export function loadCountries(params = {}) {
	return api.get(`${API_URL.COUNTRY}`, { params });
};

export function loadStates(params = {}) {
	return api.get(`${API_URL.STATE}`, { params });
};

export function loadCities(params = {}) {
	return api.get(`${API_URL.CITY}`, { params });
};

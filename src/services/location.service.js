import { axios as api } from '../common/helpers/api';
import { API_URL } from '../common/configs/api';

export function loadCities(params = {}) {
	return api.get(`${API_URL.LOCATION_CITY}`, { params });
};

export function loadDistricts(params = {}) {
	return api.get(`${API_URL.LOCATION_DISTRICT}`, { params });
};

import { axios as api } from '../common/helpers/api';
import { API_URL } from '../common/configs/api';

export const getProfile = () => api.get(API_URL.PROFILE);

export function getMe() {
	return api.get(API_URL.PROFILE);
};

export function updateProfile(formData = {}) {
	return api.put(API_URL.PROFILE, formData, { useFormData: true });
};

export function getAvatar() {
	return api.get(API_URL.AVATAR);
};

import $events from 'events';
import * as services from '@/services';
import { ACTIONS, MUTATES } from './const';
import storage from '../common/helpers/storage/localStorage';

export const actions = {
	[ACTIONS.LOAD]: ({ commit, state }) => {
		commit(MUTATES.LOADING);
		commit(MUTATES.LOADED);
	},

	[ACTIONS.SET_SKIN]: ({ commit }, skin) => {
		commit(MUTATES.SET_SKIN, skin);
	},

	[ACTIONS.LOGIN]: ({ commit }, user) => {
		// commit(types.SET_SKIN, skin);
	},

	[ACTIONS.REGISTER]: ({ commit }, user) => {
		return services.signup(user).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[ACTIONS.SIGNIN]: ({ commit }, login) => {
		return services.signin(login).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			storage.set('token', res.data.token);
			storage.set('user', res.data.user);
			commit(MUTATES.SET_CURRENT_USER, res.data.user);
			return { success: true };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[ACTIONS.SIGNOUT]: ({ commit }) => {
		return services.signout().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}

			storage.remove('user');
			storage.remove('token');
			commit(MUTATES.SET_CURRENT_USER, {});
			return { success: true };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[ACTIONS.LOAD_ME]: ({ commit }) => {
		return services.getMe().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			storage.set('user', res.data);
			commit(MUTATES.SET_CURRENT_USER, res.data);
			return { success: true };
		}).catch(res => {
			storage.set('user', {});
			return { errors: res.error };
		});
	},

	[ACTIONS.SET_TITLE_PAGE]: ({ commit }, title) => {
		$events.$emit("setComTitle", title);
	},

	[ACTIONS.UPDATE_PROFILE]: ({ commit }, formData) => {
		return services.updateProfile(formData).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}

			commit(MUTATES.SET_PROFILE, res.data);
			return { success: true };
		})
			.catch(res => {
				return {
					errors: res.error
				};
			});
	},

	[ACTIONS.GET_AVATAR]: ({ commit }) => {
		return services.getAvatar().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true };
		})
			.catch(res => {
				return {
					errors: res.error
				};
			});
	},

	[ACTIONS.GET_TIME_ZONES]: ({ commit }, query = {}) => {
		return services.loadTimeZones(query).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return {data: res.data};
		}).catch(res => {
			return {
				errors: res.error
			};
		});
	},

	[ACTIONS.GET_COUNTRIES]: ({ commit }, query = {}) => {
		return services.loadCountries(query).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return {data: res.data};
		}).catch(res => {
			return {
				errors: res.error
			};
		});
	},

	[ACTIONS.GET_STATES]: ({ commit }, query = {}) => {
		return services.loadStates(query).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return {data: res.data};
		}).catch(res => {
			return {
				errors: res.error
			};
		});
	},

	[ACTIONS.GET_CITIES]: ({ commit }, query = {}) => {
		return services.loadCities(query).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return {data: res.data};
		}).catch(res => {
			return {
				errors: res.error
			};
		});
	},
};

/* GETTERS - START */
export const GETTERS = {
	IS_LOADING: 'isLoading',
	UNREAD_MSG_COUNT: 'unreadMessagesCount',
	UNREAD_NOTICE_COUNT: 'unreadNotificationsCount',
	CURRENT_USER: 'currentUser',
	AVATAR: 'avatar',
	SKIN: 'skin',
	IS_AUTHENTICATED: 'isAuthenticated',
	LOADING: 'loading',
	SAVING: 'saving'
};
/* GETTERS - END */

/* ACTIONS - START */
export const ACTIONS = {
	GET_AVATAR: 'getAvatar',
	UPDATE_PROFILE: 'updateProfile',
	SET_TITLE_PAGE: 'setPageTitle',
	LOAD_ME: 'loadMe',
	SIGNOUT: 'signout',
	SIGNIN: 'signin',
	REGISTER: 'register',
	LOGIN: 'login',
	SET_SKIN: 'setSkin',
	LOAD: 'load',
	GET_TIME_ZONES: 'getTimeZones',
	GET_COUNTRIES: 'getCountries',
	GET_STATES: 'getStates',
	GET_CITIES: 'getCities'
};
/* ACTIONS - END */

/* MUTATIONS - START */
export const MUTATES = {
	TOGGLE_LOADING: 'toggleLoading',
	SET_CURRENT_USER: 'setCurrentUser',
	SET_PROFILE: 'setProfile',
	SET_AVATAR: 'setAvatar',
	LOADING: 'loading',
	LOADED: 'loaded',
	SAVING: 'saving',
	SAVED: 'saved',
	SET_PAGINATION: 'setPagination'
};
/* MUTATIONS - END */

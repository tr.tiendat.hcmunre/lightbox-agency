import * as adApi from '../../../services/ad.service';
import * as locApi from '../../../services/location.service';

import AD from './const';

export const actions = {
	[AD._ACTIONS.LOAD]: ({ commit }, id) => {
		return adApi.loadAd(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, ad: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[AD._ACTIONS.REMOVE]: ({ commit }, id) => {
		return adApi.removeAd(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[AD._ACTIONS.EDIT_AD]: ({ commit }, ad) => {
		let req = ad._id ? adApi.updateAd(ad._id, ad) : adApi.addAd(ad);
		return req.then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, client: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[AD._ACTIONS.LOAD_ALL]: ({ commit }) => {
		return adApi.loadAllAd().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, ads: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[AD._ACTIONS.LOAD_CITY]: ({ commit }) => {
		return locApi.loadCities().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, cities: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[AD._ACTIONS.LOAD_DISTRICT]: ({ commit }, query = { city: '' }) => {
		return locApi.loadDistricts(query).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, districts: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[AD._ACTIONS.REMOVE_ITEMS]: ({commit}, idAdList) => {
		return adApi.removeAdItems(idAdList).then(res => {
			if (res.error) {
				return {
					errors: res.errors || res.error
				};
			}
			return {
				success: true
			};
		});
	},

	[AD._ACTIONS.UPDATE_STATUS]: ({commit}, data) => {
		const { id, status } = data;
		return adApi.updateStatus(id, status).then(response => {
			if (response.error) {
				return {
					errors: response.errors || response.error
				};
			}
			return {
				success: true
			};
		});
	},

	[AD._ACTIONS.LOAD_DEVICES]: () => {

		return adApi.getDeviceAvalables().then(res => {

			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, devices: res.data };
		});
	}
};

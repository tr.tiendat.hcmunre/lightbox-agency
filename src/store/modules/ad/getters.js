import AD from "./const";

export const getters = {
	[AD._GETTERS.USERS]: state => state.users,
};

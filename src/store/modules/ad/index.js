import { state } from './state';
import { actions } from './actions';
import { mutations } from './mutations';
import { getters } from './getters';
import location, { MODULE_NAME as LOCATION_MODULE_NAME } from './location';
import time, { MODULE_NAME as TIME_MODULE_NAME } from './time';

export default {
	namespaced: true,
	state,
	actions,
	getters,
	mutations,
	modules: {
		[LOCATION_MODULE_NAME]: location,
		[TIME_MODULE_NAME]: time
	}
};

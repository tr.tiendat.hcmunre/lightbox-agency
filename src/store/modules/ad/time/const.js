import { MODULE_NAME as PARENT_MODULE_NAME } from '../const';

export const MODULE_NAME = 'datetime.js';
const _PREFIX = `${PARENT_MODULE_NAME}/${MODULE_NAME}`;

const _GETTERS = {
	USERS: 'users'
};

export const GETTERS = {
	USERS: `${_PREFIX}/${_GETTERS.USERS}`
};

const _ACTIONS = {
	LOAD: 'load',
	REMOVE: 'remove',
	EDIT: 'edit',
	LOAD_ALL: 'loadAll'
};

export const ACTIONS = {
	LOAD: `${_PREFIX}/${_ACTIONS.LOAD}`,
	REMOVE: `${_PREFIX}/${_ACTIONS.REMOVE}`,
	EDIT: `${_PREFIX}/${_ACTIONS.EDIT}`,
	LOAD_ALL: `${_PREFIX}/${_ACTIONS.LOAD_ALL}`
};

const _MUTATORS = {
	SET_USERS: 'setUsers'
};

export const MUTATORS = {
	SET_USERS: `${_PREFIX}/${_MUTATORS.SET_USERS}`
};

export default {
	_GETTERS,
	_ACTIONS,
	_MUTATORS
};

import _ from 'lodash';
import TIME from './const';

export const mutations = {
	[TIME._MUTATORS.SET_USERS]: (state, users) => {
		state.users = _.uniqBy(state.users, users, '_id');
	}
};

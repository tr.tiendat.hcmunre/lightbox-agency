import * as clientApi from '@/services/client.service';
import CLIENT from './const';

export const actions = {
	[CLIENT._ACTIONS.LOAD]: ({ commit }, id) => {
		return clientApi.loadClient(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, client: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[CLIENT._ACTIONS.REMOVE]: ({ commit }, id) => {
		return clientApi.removeClient(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[CLIENT._ACTIONS.EDIT_CLIENT]: ({ commit }, client) => {
		let req = client._id ? clientApi.updateClient(client._id, client) : clientApi.addClient(client);
		return req.then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, client: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[CLIENT._ACTIONS.LOAD_ALL]: ({ commit }) => {
		return clientApi.loadClients().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, clients: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	}
};

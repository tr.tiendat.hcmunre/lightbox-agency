export const MODULE_NAME = 'client';

const _GETTERS = {
	USERS: 'users'
};

export const GETTERS = {
	USERS: `${MODULE_NAME}/${_GETTERS.USERS}`
};

const _ACTIONS = {
	LOAD: 'load',
	REMOVE: 'remove',
	EDIT_CLIENT: 'editClient',
	LOAD_ALL: 'loadAll'
};

export const ACTIONS = {
	LOAD: `${MODULE_NAME}/${_ACTIONS.LOAD}`,
	REMOVE: `${MODULE_NAME}/${_ACTIONS.REMOVE}`,
	EDIT_CLIENT: `${MODULE_NAME}/${_ACTIONS.EDIT_CLIENT}`,
	LOAD_ALL: `${MODULE_NAME}/${_ACTIONS.LOAD_ALL}`
};

const _MUTATES = {
	SET_USERS: 'setUsers'
};

export const MUTATES = {
	SET_USERS: `${MODULE_NAME}/${_MUTATES.SET_USERS}`
};

export default {
	_GETTERS,
	_ACTIONS,
	_MUTATES
};

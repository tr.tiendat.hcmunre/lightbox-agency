export const MODULE_NAME = 'notifications';

const _GETTERS = {
	NOTIFIES: 'notifies',
};

export const GETTERS = {
	NOTIFIES: `${MODULE_NAME}/${_GETTERS.NOTIFIES}`,
};

const _ACTIONS = {
	ADD_NOTIFY: 'addNotify',
};

export const ACTIONS = {
	ADD_NOTIFY: `${MODULE_NAME}/${_ACTIONS.ADD_NOTIFY}`,
};

const _MUTATES = {
	ADD_NOTIFY: 'addNotify',
};

export const MUTATES = {
	ADD_NOTIFY: `${MODULE_NAME}/${_MUTATES.ADD_NOTIFY}`,
};

export default {
	_GETTERS,
	_ACTIONS,
	_MUTATES
};

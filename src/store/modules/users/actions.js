import USERS from "./const";
import * as services from '@/services';

export const actions = {
	[USERS._ACTIONS.SET_USER]: ({commit}, user) => {
		commit(USERS._MUTATES.SET_USER, user);
	},

	[USERS._ACTIONS.CHECK_USER_EXISTS]: ({ commit }, email = '') => {
		return services.checkExists(email).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return res.data;
		}).catch(res => {
			return {
				errors: res.error
			};
		});
	}
};

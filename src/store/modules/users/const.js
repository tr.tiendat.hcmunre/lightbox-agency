export const MODULE_NAME = 'users';

const _GETTERS = {
	USERS: 'users'
};

export const GETTERS = {
	USERS: `${MODULE_NAME}/${_GETTERS.USERS}`
};

const _ACTIONS = {
	SET_USER: 'setUsers',
	CHECK_USER_EXISTS: 'checkUserExist'
};

export const ACTIONS = {
	SET_USER: `${MODULE_NAME}/${_ACTIONS.SET_USER}`,
	CHECK_USER_EXISTS: `${MODULE_NAME}/${_ACTIONS.CHECK_USER_EXISTS}`
};

const _MUTATES = {
	SET_USER: 'setUsers',
	SET_AUTH: 'setAuth',
	SET_PROFILE: 'setProfile',
};

export const MUTATES = {
	SET_USER: `${MODULE_NAME}/${_MUTATES.SET_USER}`,
	SET_AUTH: `${MODULE_NAME}/${_MUTATES.SET_USER}`,
	SET_PROFILE: `${MODULE_NAME}/${_MUTATES.SET_PROFILE}`,
};

export default {
	_GETTERS,
	_ACTIONS,
	_MUTATES
};

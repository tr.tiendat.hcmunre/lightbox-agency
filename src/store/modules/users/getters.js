import { GETTERS } from "./const";

export const getters = {
	[GETTERS.USERS]: state => state.users
};

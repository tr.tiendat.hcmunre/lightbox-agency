import { MUTATES } from './const';
import storage from '../common/helpers/storage/localStorage';

export const mutations = {
	[MUTATES.SET_CURRENT_USER]: (state, user) => {
		storage.set('user', user);
		state.currentUser = user;
	},

	[MUTATES.SET_PROFILE]: (state, user) => {
		storage.set('user', user);
		state.currentUser = user;
	},

	[MUTATES.SET_AVATAR]: (state, payload) => {
		state.avatar = payload;
	},

	[MUTATES.LOADING]: (state) => {
		state.loading = true;
	},

	[MUTATES.LOADED]: (state) => {
		state.loading = false;
	},

	[MUTATES.SAVING]: (state) => {
		state.saving = true;
	},

	[MUTATES.SAVED]: (state) => {
		state.saving = false;
	},

	[MUTATES.SET_PAGINATION]: (state, pagination) => {
		state.pagination = pagination;
	}
};

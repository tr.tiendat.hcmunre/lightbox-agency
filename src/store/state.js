import { BASE_API_URL } from '@/common/configs/api';
// import storage from '../common/helpers/storage/localStorage';

export const state = {
	loading: false,
	saving: false,
	loadError: null,
	currentUser: {},
	avatar: `${BASE_API_URL}/avatar?_=${Date.now()}`,
};

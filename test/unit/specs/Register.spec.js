import Vue from 'vue';
import Register from '@/pages/Register/Register.vue';
import { shallowMount } from '@vue/test-utils';
import store from '@/vuex/app';

// const localVue = createLocalVue();
// localVue.use(Vuex);

describe('Page Register.vue', () => {
	it('should render correct contents', () => {
		// const Constructor = Vue.extend(Register);
		// const vm = new Constructor().$mount();
		const wrapper = shallowMount(Register, { store, Vue });
		// const vm = wrapper.vm;
		expect(wrapper.find('.register-box-msg').text()).to.contain('Register a new membership');
		expect(wrapper.find('a.btn').text()).contain('Register');
	});
});

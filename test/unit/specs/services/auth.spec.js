import * as auth from '@/service/auth.service';

describe('Auth service', () => {
	it('should sign in success', async () => {
        let result = await auth.signin({email: 'dientran@success-ss.com.vn', password: '123456789'});
        console.log(result);
        expect(result).to.be.a('object');
	});
});